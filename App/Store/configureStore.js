import {
  createStore,
  applyMiddleware
} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { persistStore } from 'redux-persist';
import ScreenTracking from './ScreenTrackingMiddleware';
import DebugConfig from '../Config/DebugConfig';
import ReduxPersist from '../Config/ReduxPersist';
import rootReducer from '../Reducers';

export default () => {
  const middleware = []
  middleware.push(ScreenTracking);
  middleware.push(thunk);
  if (DebugConfig.reduxLogging) {
    middleware.push(logger)
  }
  const createAppropriateStore = DebugConfig.useReactotron ? console.tron.createStore : createStore

  const store = createAppropriateStore(rootReducer, undefined, applyMiddleware(...middleware))
  const persistor = persistStore(
    store,
    null,
    () => {
      store.getState() // if you want to get restoredState
    }
  )
 // persistor.purge;
  if (ReduxPersist.active) {
    //Rehydration.updateReducers(store)
  }
  return { store, persistor };
};


