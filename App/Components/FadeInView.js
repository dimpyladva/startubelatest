import React from 'react';
import { Animated, Text, View } from 'react-native';

export default class FadeInView extends React.Component {
    state = {
        fadeAnim: new Animated.Value(0.5),
        // scale: 100,
        // rotateY: 30,
        // Initial value for opacity: 0
    }

    componentDidMount() {
        Animated.timing(                  // Animate over time
            this.state.fadeAnim,            // The animated value to drive
            {
                toValue: 1,                   // Animate to opacity: 1 (opaque)
                duration: 2000,
                useNativeDriver: true,             // Make it take a while
            }
        ).start();
        // Starts the animation
    }

    render() {
        let { fadeAnim } = this.state;

        return (




            <Animated.View                 // Special animatable View
                style={{
                    ...this.props.style,
                    opacity: fadeAnim,         // Bind opacity to animated value
                }}
            >
                {this.props.children}
            </Animated.View>
        );
    }
}
{/* <Animated.View
style={{
    transform: [
        { scale: this.state.scale },
        { rotateY: this.state.rotateY },
        { perspective: 1000 }, // without this line this Animation will not render on Android while working fine on iOS
    ],
}}
/> */}