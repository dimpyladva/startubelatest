/**
* This is the navbar component
* example of usage:
*   var left = (<Left><Button transparent><Icon name='menu' /></Button></Left>);
*   var right = (<Right><Button transparent><Icon name='menu' /></Button></Right>);
*   <Navbar left={left} right={right} title="My Navbar" />
**/

// React native and others libraries imports
import React, { Component } from 'react';
import { Header, Body, Title, Left, Right, Icon, Text } from 'native-base';
// Our custom files and classes import
import { Colors, ApplicationStyles } from "../Themes";
import styles from "../Themes/styles";

export default class AppHeader extends Component {
  render() {

    return (
      <Header
        style={{ backgroundColor: Colors.navbarBackgroundColor }}
        backgroundColor={Colors.navbarBackgroundColor}
        androidStatusBarColor={Colors.statusBarColor}
        style={ApplicationStyles.header}
        noShadow={true}
      >
        {this.props.left ? this.props.left : <Left style={{ flex: 1, padding: 5, margin: 5 }} />}
        <Body style={styles.body}>
          <Text style={styles.textTitle}>{this.props.title}</Text>
        </Body>
        {this.props.right ? this.props.right : <Right style={{ flex: 1, padding: 5, margin: 5 }} />}
      </Header>
    );
  }
}
