/**
 * This is the Product component
 **/

// React native and others libraries imports
import React, { Component } from "react";
import { TouchableOpacity } from "react-native";
import { CachedImage } from 'react-native-cached-image';
import {
    View,
    Text
} from "native-base";
//import { Actions } from "react-native-router-flux";
import styles from '../Themes/styles';
import { Images } from "../Themes/";
export default class ArtistView extends Component {

    render() {
        var rowData = this.props.data;
        return (
            <TouchableOpacity
                onPress={() => this.pressed()} >
                <View style={styles.marginLow}>
                    <CachedImage
                        source={{
                            uri: rowData.image
                        }}
                        activityIndicatorProps = {styles.imageActivityIndicator}
                        defaultSource={Images.defaultImageUser}
                        fallbackSource={Images.defaultImageUser}
                        style={styles.connectionUserImg}
                    />
                    <Text style={styles.nameTxt}>{rowData.artist_name}</Text>
                    <Text style={styles.designationTxt}>{rowData.artist_role_name}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    pressed() {
        this.props.navigation.navigate('ArtistDetail', { artistData: this.props.data, title: this.props.data.artist_name });

    }
}


