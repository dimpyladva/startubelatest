/**
 * This is the category component used in the home page
 **/

// React native and others libraries imports
import React, { Component } from "react";
import { TouchableOpacity, Text, ImageBackground } from "react-native";
import { View, Card, CardItem, Button } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
// Our custom files and classes import
import styles from "../Theme/style";
import string from "../Theme/string";
import color from "../Theme/color";
import { Share } from "react-native";
export default class VideoItemBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showVideo: false
    };
  }
  render() {
    var vDetails = this.props.video;
    return (
      <Card style={{ flex: 1, margin: 0.5, shadowColor: "black" }}>
        <TouchableOpacity
          // onPress={this._onPress.bind(this)}
          activeOpacity={0.5}
        >
          <CardItem cardBody>
            <ImageBackground
              style={styles.videoCatalougeImage}
              source={{ uri: vDetails.thumbnail }}
            >
              <Button
                transparent
                style={styles.playIconContainer}
                onPress={() => this.setState({ showVideo: true })}
              >
                <Icon size={50} name="play-circle" color="white" />
              </Button>
            </ImageBackground>
          </CardItem>
        </TouchableOpacity>
      </Card>
    );
  }
  // renderYoutubePlayer() {
  //   var vDetails = this.props.video;

  //   return (
  //     <View >
  //       <YouTube
  //         videoId={vDetails.video_id} // The YouTube video ID
  //         play={false} // control playback of video with true/false
  //         fullscreen={false} // control whether the video should play in fullscreen or inline
  //         loop={false} // control whether the video should loop when ended
  //         apiKey="AIzaSyDobd_4OKUfDaazchfZXyu_H93U8Y_MEEc"
  //         showFullscreenButton={true}
  //         showinfo={true}
  //         style={{ alignSelf: "stretch", height: 300 }}
  //       // onReady={e => this.setState({ isReady: true })}
  //       // onChangeState={e => this.setState({ status: e.state })}
  //       // onChangeQuality={e => this.setState({ quality: e.quality })}
  //       // onError={e => this.setState({ error: e.error })}

  //       />
  //       {this.renderName(vDetails)}
  //     </View>);
  // }
  shareVideo() {
    Share.share(
      {
        message: this.props.video.url,
        url: this.props.video.url,
        title: this.props.video.title
      },
      {
        // Android only:
        dialogTitle: "Share " + this.props.video.title,
        // iOS only:
        excludedActivityTypes: ["com.apple.UIKit.activity.PostToTwitter"]
      }
    );
  }
  showVideo() {
    if (
      this.props.video.video_id != null &&
      this.props.video.video_id.length > 0
    ) {
      this.props.navigation.navigate("fullVideo", {
        videoId: this.props.video.video_id
      });
    } else {
      alert(string.videodoesnotExists);
    }
  }
  // _onPress() {
  //   this.props.navigation.navigate('product');
  //   // Actions.productlist({
  //   //   categoryId: this.props.id,
  //   //   categoryName: this.props.title,
  //   //   title: this.props.title
  //   // });
  // }
}
