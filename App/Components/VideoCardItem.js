/**
 * This is the Product component
 **/

// React native and others libraries imports
import React, { Component } from "react";
import {  TouchableOpacity,Share } from "react-native";
import {
  View,
  Col,
  Card,
  CardItem,
  Text,
  Body,
  Button,
  Right,
  Icon
} from "native-base";
import styles from '../Themes/styles';
import { Images } from "../Themes";
import { CachedImage } from 'react-native-cached-image';
import string from "../Themes/String";
const fallbacks = [
  Images.defaultImage
];
export default class VideoCardItem extends Component {
  render() {
    var data = this.props.data;
    const { isSmall = false } = this.props;
    return (

      <View style={!isSmall ? styles.cardView : styles.cardViewSmall}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("VideoDetail", { videoItem: data })}>
          <CachedImage
            source={{
              uri: data.thumbnail
            }}
            fallbackSource={Images.defaultImage}
            defaultSource={Images.defaultImage}
            activityIndicatorProps = {styles.imageActivityIndicator}
            style={!isSmall ? styles.cardImage : styles.cardImageSmall}
          />
        </TouchableOpacity>

        <View style={!isSmall ? styles.cardContent : styles.cardContentSmall}>
          <View style={styles.moreContainer}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("VideoDetail", { videoItem: data })}>
              <Text
                style={styles.nameTitle}
                numberOfLines={2}
              >{data.name}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.shareVideo(data)}>
              <Icon name="md-share" style={styles.moreIcon} />
            </TouchableOpacity>

          </View>

        </View>
      </View>

    );
  }

  shareVideo(data) {
    
    Share.share(
      {
        message: data.url,
        url: data.url,
        title: data.name
      },
      {
        // Android only:
        dialogTitle: 'Share ' + data.name,
        // iOS only:
        excludedActivityTypes: ["com.apple.UIKit.activity.PostToTwitter"]
      }
    );
  }
}

const style = {
  button: { flex: 1, height: null },
  image: { height: 90, width: 160, flex: 1 },
  leftMargin: {
    marginLeft: 7,
    marginRight: 0,
    marginBottom: 7
  },
  rightMargin: {
    marginLeft: 0,
    marginRight: 7,
    marginBottom: 7
  },
  border: {
    position: "absolute",
    top: 10,
    left: 10,
    right: 10,
    bottom: 10,
    borderWidth: 1,
    borderColor: "rgba(253, 253, 253, 0.2)"
  },
  price: {
    fontSize: 11,

    paddingRight: 5,
    zIndex: 1000,
    backgroundColor: "#fdfdfd"
  },
  line: {
    width: "100%",
    height: 1,
    backgroundColor: "#7f8c8d",
    position: "absolute",
    top: "52%"
  }
};
