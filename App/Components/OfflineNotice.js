import React, { PureComponent } from "react";
import { View, Text, StyleSheet } from "react-native";
import Metrics from "../Themes/Metrics";
import Colors from "../Themes/Colors";
const { width } = Metrics.WIDTH;

function MiniOfflineSign() {
  return (
    <View style={styles.offlineContainer}>
      <Text style={styles.offlineText}>No Internet Connection</Text>
    </View>
  );
}

class OfflineNotice extends PureComponent {
  render() {
    if (!this.props.isConnected) {
      return <MiniOfflineSign />;
    }
    return null;
  }
}

const styles = StyleSheet.create({
  offlineContainer: {
    backgroundColor: Colors.navbarBackgroundColor,
    height: 30,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    width,
    position: "relative",
    zIndex: 90
  },
  offlineText: { color: Colors.white }
});

export default OfflineNotice;
