/**
 * This is the Product component
 **/

// React native and others libraries imports
import React, { Component } from "react";
import { TouchableOpacity } from "react-native";
import { CachedImage } from 'react-native-cached-image';
import {
    View,
    Text
} from "native-base";
//import { Actions } from "react-native-router-flux";
import styles from '../Themes/styles';
import { Images } from "../Themes/";
export default class ArtistSimpleView extends Component {

    render() {

        var rowData = this.props.data;
        var artistname = rowData.name;
        if (rowData.name.length > 11) {
            const words = rowData.name.split(' ');
            artistname = (words[0]);
        }
        return (
            <TouchableOpacity
                onPress={() => this.pressed()} >
                <View style={[styles.marginLow, styles.connectionUserImgContainer]}>
                    <CachedImage
                        source={{
                            uri: rowData.image
                        }}
                        activityIndicatorProps = {styles.imageActivityIndicator}
                        defaultSource={Images.defaultImageUser}
                        style={styles.connectionUserImg}
                    />
                    <Text style={styles.nameTxt} numberOfLines={1} >{artistname}</Text>

                </View>
            </TouchableOpacity >
        );
    }

    pressed() {
        this.props.navigation.navigate('ArtistDetail', { artistData: this.props.data, title: this.props.data.name });

    }
}


