// import React, { Component } from 'react'
// import { View, StatusBar } from 'react-native'
// import { connect } from 'react-redux'
// import Orientation from 'react-native-orientation-locker'
// import NetInfo from '@react-native-community/netinfo'
// import ReduxNavigation from '../Navigation/ReduxNavigation'
// import StartupActions from '../Redux/StartupRedux'
// import SettingsActions from '../Redux/SettingsRedux'
// import ErrorLogActions from '../Redux/ErrorLogRedux'
// import ReduxPersist from '../Config/ReduxPersist'
// import { Colors } from '../Themes'
// import styles from './Styles/RootContainerStyles'
// // Error handle
// // import { Alert } from 'react-native'
// // import { BackAndroid } from "react-native";
// // import RNRestart from 'react-native-restart'
// // import {
// //   setJSExceptionHandler,
// //   setNativeExceptionHandler,
// // } from 'react-native-exception-handler';
// // //Error Handling
// // const errorHandler = (e, isFatal) => {
// //   console.log(e);
// //   console.log(isFatal);
// //   if (isFatal) {
// //     this.props.errorLogRequest({ Exception: e.name, Message: e.message })
// //     Alert.alert(
// //       'Sorry! Unexpected error occured.',
// //       'We took a note of error and will resolve at earliest. Please continue to explore the rest of the features.',
// //       [
// //         // {
// //         //   text: 'Quite',
// //         //   onPress: () => BackAndroid.exitApp(),
// //         //   style: 'cancel',
// //         // },
// //         {
// //           text: 'Relaunch the App', onPress: () =>
// //             RNRestart.Restart()
// //         },
// //       ],
// //       { cancelable: false },
// //     );
// //     // Alert.alert(
// //     //   'Unexpected error occurred From App.js',
// //     //   `
// //     //     Error: ${(isFatal) ? 'Fatal:' : ''} ${e.name} ${e.message}
// //     //     We have reported this to our team ! Please close the app and start again!
// //     //     `,
// //     //   [{
// //     //     text: 'Close'
// //     //   }]
// //     // );
// //   } else {
// //     console.log(e); // So that we can see it in the ADB logs in case of Android if needed
// //   }
// // };
// // setJSExceptionHandler(errorHandler, true);

// // setNativeExceptionHandler((errorString) => {
// //   console.log('setNativeExceptionHandler');
// // });

// class RootContainer extends Component {
//   componentDidMount() {
//     if (!ReduxPersist.active) {
//       this.props.startup()
//     }

//     const initial = Orientation.getInitialOrientation()
//     this.props.updateSettings({
//       orientation: initial
//     })

//     Orientation.addOrientationListener(this.orientationDidChange)
//     NetInfo.isConnected.fetch().then((isConnected) => {
//       this.props.updateSettings({
//         isConnected
//       })
//     })
//     NetInfo.isConnected.addEventListener('connectionChange', (isConnected) => {
//       this.props.updateSettings({
//         isConnected
//       })
//     })
//   }

//   componentWillUnmount() {
//     Orientation.removeOrientationListener(this.orientationDidChange)
//     NetInfo.isConnected.removeEventListener(
//       'connectionChange',
//       this.props.updateInternetStatusAction
//     )
//   }

//   orientationDidChange = (orientation) => {
//     this.props.updateSettings({
//       orientation
//     })
//   }

//   render() {
//     return (
//       <View style={styles.applicationView}>
//         <StatusBar
//           backgroundColor={Colors.background}
//           barStyle='light-content' />
//         <ReduxNavigation />
//       </View>
//     )
//   }
// }

// const mapStateToProps = (state) => {
//   return {
//     isConnected: state.settings.isConnected
//   }
// }
// const mapDispatchToProps = (dispatch) => ({
//   startup: () => dispatch(StartupActions.startup()),
//   updateSettings: (data) => dispatch(SettingsActions.updateSettings(data)),
//   errorLogRequest: (data) => dispatch(ErrorLogActions.errorLogRequest(data))
// })

// export default connect(mapStateToProps, mapDispatchToProps)(RootContainer)




import React, { Component } from "react";
import { View, StatusBar } from "react-native";
import NetInfo from "@react-native-community/netinfo";
// import { AppWithNavigationState } from "../Navigation/ReduxNavigation";
import ReduxNavigation from '../Navigation/ReduxNavigation'
import {
  setOrientationChange,
  updateInternetStatusAction
} from "../Actions/ServiceCallAction";
import { connect } from "react-redux";
import Orientation from "react-native-orientation";
import OfflineNotice from "../Components/OfflineNotice";
import styles from "./Styles/RootContainerStyles";
class RootContainer extends Component {
  constructor(props) {
    super(props);
    //const initial = Orientation.getInitialOrientation();
    //console.log("INitial"+initial)
    //this.props.setOrientationChange(initial)
  }

  componentDidMount() {
    NetInfo.fetch().then(isConnected => {
      console.log("First, is " + (isConnected ? "online" : "offline"));
      this.props.updateInternetStatusAction(isConnected);
    });
    NetInfo.addEventListener("connectionChange", connected => {
      console.log("connected" + connected);
      this.props.updateInternetStatusAction(connected);
    });
    // this locks the view to Portrait Mode
    Orientation.lockToPortrait();
    //Orientation.unlockAllOrientations();
    //Orientation.addOrientationListener(this.props.setOrientationChange);
  }
  componentWillUnmount() {
    //console.log("Root Unmount")
    // NetInfo.removeEventListener(
    //   "connectionChange",
    //   this.props.updateInternetStatusAction
    // );
    // Orientation.removeOrientationListener(this.props.setOrientationChange);
  }
  render() {
    return (
      <View style={styles.applicationView}>
        <StatusBar backgroundColor="#b72b4b" barStyle="light-content" />
        <ReduxNavigation />
        <OfflineNotice {...this.props} />
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  setOrientationChange: orientation =>
    dispatch(setOrientationChange(orientation)),
  updateInternetStatusAction: isConnected =>
    dispatch(updateInternetStatusAction(isConnected))
});
const mapStateToProps = state => ({
  currentOrientation: state.serviceReducer.currentOrientation,
  orientationChanged: state.serviceReducer.orientationChanged,
  isConnected: state.serviceReducer.isConnected
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RootContainer);
