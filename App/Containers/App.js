
import React from 'react'
import 'react-native-browser-polyfill';
import { Provider } from 'react-redux';
import DebugConfig from '../Config/DebugConfig'
import configureStore from '../Store/configureStore';
import RootContainer from './RootContainer'
import { PersistGate } from 'redux-persist/lib/integration/react'

const { store, persistor } = configureStore()
const App = () => (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <RootContainer />
      </PersistGate>
    </Provider>
);

export default DebugConfig.useReactotron
  ? console.tron.overlay(App)
  : App
