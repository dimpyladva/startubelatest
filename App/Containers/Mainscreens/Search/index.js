import React, { Component } from "react";
import {
  TouchableOpacity,
  View,
  FlatList,
  ActivityIndicator,
  Text,
  BackHandler,
  StatusBar,
  Keyboard,
  TextInput
} from "react-native";
import { CachedImage } from 'react-native-cached-image';
import { connect } from "react-redux";
import {
  Container,
  Content,
  Icon,
  Header,
  Item,
  Button,
  Input,
  Left,
  Right,
  Card,
  Body,
  Col
} from "native-base";
import AppHeader from "../../../Components/AppHeader";
import { searchSuggest, search, searchVideo, searchArtist } from "../../../Actions/ServiceCallAction";
import VideoCardItem from "../../../Components/VideoCardItem";
import { Colors, Images, ApplicationStyles, Fonts, Metrics } from "../../../Themes/";
import styles from "../../../Themes/styles";
class Search extends Component {
  constructor(props) {
    super(props);
    queryDelay = 1000;
    queryTimeout = -1;
    this.state = {
      isLoading: false,
      isLoadingMore: false,
      isSearch: true,
      keyword: "",
      showVideos: true,
      showArtist: false,
      showSuggestion: false,
      showSearchView: false,
      searchSuggestions: []
    }
    this.loadVideos = this.loadVideos.bind(this);
    this.loadArtists = this.loadArtists.bind(this);
  }
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
  }
  componentDidUpdate(prevPros) {
    if (prevPros.searchSuggestions != this.props.searchSuggestions && !this.props.isLoading) {
      this.setState({
        searchSuggestions: this.props.searchSuggestions
      })
    }
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }
  handleBackPress = () => {
    this.props.navigation.goBack(); // works best when the goBack is async
    return true;
  };
  onSearchClick() {
    this.setState({ showSuggestion: false, showSearchView: true, isFirstTimeVideo: true });
    this.loadVideos(1);
    this.loadArtists(1)
  }
  suggestionItemClick(data) {
    if (data.type == "a") {
      this.props.navigation.navigate("ArtistDetail", {
        artistData: data,
        title: data.name
      });
    } else {
      this.props.navigation.navigate("VideoDetail", {
        videoItem: data,
        videoId: data.id
      });
    }
  }
  viewArtistDetails(data) {
    this.props.navigation.navigate("ArtistDetail", {
      artistData: data,
      title: data.name
    });
  }
  _keyboardDidShow() {
    //  alert('Keyboard Shown');
  }

  _keyboardDidHide() {
    //  alert('Keyboard Hidden');
  }

  searchListItemView(data) {
    return (
      <TouchableOpacity onPress={() => this.suggestionItemClick(data)} style={{
        flexDirection: "row",
        paddingVertical: Metrics.WIDTH * 0.03,
        paddingHorizontal: Metrics.WIDTH * 0.04,

        borderTopWidth: 1,
        borderTopColor: Colors.backgrey
      }}>
        <Left style={{ flex: 0.85 }}>
          <Text style={styles.searchSuggestionNameTxt}>{data.name}</Text>
        </Left>
        <Right style={{ flex: 0.15 }}>
          <Text style={styles.searchSuggestionTypeTxt}>
            {data.type_label}
          </Text>
        </Right>
      </TouchableOpacity>
    );
  }
  _onChangeSearchText = data => {
    keyword = data;
    this.setState({
      keyword: keyword,
      showSuggestion: true,
      showSearchView: false
    }, () => {
      this.resetTimer();
    });
  }
  resetTimer = function () {
    let self = this;
    if (self.state.keyword.length >= 3) {
      self.props.searchSuggest(self.state.keyword, self.props.language_id);
    }
    else {
      self.setState({
        searchSuggestions: []
      })
    }
  };
  loadVideos(page = this.props.searchVideosPage, isRefreshOnly = false) {
    this.props.searchVideo(this.state.keyword, this.props.language_id, page);
  }
  loadArtists(page = this.props.searchArtistsPage, isRefreshOnly = false) {

    this.props.searchArtist(this.state.keyword, this.props.language_id, page);
  }
  renderFooter = () => {
    // console.warn(this.state.page + " last:" + this.state.last_page);

    if (this.state.showVideos && !this.props.isSearchVideoLoading && this.props.isSearchVideoEnd) return null;
    if (this.state.showArtist && !this.props.isSearchArtistLoading && this.props.isSearchArtistEnd) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator color={Colors.statusBarColor} animating size="large" />
      </View>
    );
  };
  renderSearch() {
    return (
      this.state.showSearchView && (
        <View style={{ flexDirection: "column", flex: 1 }}>
          <View style={{ flexDirection: "row" }}>
            <Button
              block
              style={this.state.showVideos ? styles.tabButtonActive : styles.tabButton}
              onPress={() =>
                this.setState({ showVideos: true, showArtist: false })
              }
            >
              <Text style={this.state.showVideos ? styles.tabBarActiveText : styles.tabBarInactiveText}>
                Video
              </Text>
            </Button>
            <Button
              block
              style={this.state.showArtist ? styles.tabButtonActive : styles.tabButton}
              onPress={() =>
                this.setState({ showVideos: false, showArtist: true })
              }
            >
              <Text style={this.state.showArtist ? styles.tabBarActiveText : styles.tabBarInactiveText}>
                Artist
              </Text>
            </Button>
          </View>
          {this.state.showVideos && (
            <FlatList
              style={{
                backgroundColor: "#e0e6f2"
              }}
              numColumns={2}
              horizontal={false}
              contentContainerStyle={styles.videoContainerStyle}
              data={this.props.searchVideos}
              renderItem={({ item: rowData }) => (
                <VideoCardItem data={rowData} isSmall={true} {...this.props} />
              )}
              keyExtractor={item => item.id}
              refreshing={this.props.isSearchRefreshVideoLoading}
              onRefresh={() => this.loadVideos(1, true)}
              onEndReached={
                () => {
                  !this.props.isSearchVideoEnd ?
                    this.loadVideos(this.props.searchVideosPage, true)
                    : null
                }
              }
              onEndThreshold={0.5}
            />
          )}
          {this.state.showArtist && (
            <FlatList
              style={{
                backgroundColor: "#e0e6f2"
              }}
              numColumns={2}
              horizontal={false}
              contentContainerStyle={styles.videoContainerStyle}
              data={this.props.searchArtists}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() => this.viewArtistDetails(item)}
                >
                  <View
                    style={styles.cardViewWhiteArtist}
                  >
                    <CachedImage
                      source={{ uri: item.image }}
                      style={styles.artistImage}
                      defaultSource={Images.defaultImageUser}
                      activityIndicatorProps={styles.imageActivityIndicator}
                    />
                    <Text style={styles.headertextSmall}>
                      {item.name}
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
              keyExtractor={item => item.id}
              refreshing={this.props.isSearchRefreshArtistLoading}
              onRefresh={() => this.loadArtists(1, true)}
              onEndReached={
                () => {
                  !this.props.isSearchArtistEnd ?
                    this.loadArtists(this.props.searchArtistsPage, true)
                    : null
                }
              }
              onEndThreshold={0.5}
            />
          )}
        </View>
      )
    );
  }
  renderSearchHeader() {
    return (
      <View style={{
        flexDirection: "row",
        marginTop: 30,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: "center",
        justifyContent: "center",
        alignContent: "center"
      }}>
        <Icon
          name="ios-arrow-back"
          style={{
            padding: 10,
            flex: 0.1,
            marginLeft: 10,
            justifyContent: "center",
          }}
          size={18}
          onPress={() => this.handleBackPress()}
        />
        <TextInput
          style={{
            flex: 0.8
          }}
          onSubmitEditing={() => this.onSearchClick()}
          placeholder="Search here (Min. 3 characters)"
          onChangeText={this._onChangeSearchText}
        />

        <Icon
          name="ios-search"
          style={{
            padding: 10,
            flex: 0.1
          }}
          size={15}
          onPress={() => this.onSearchClick()}
        />
      </View>
    );
  }
  render() {
    var that = this;
    var left = (
      <View>
        <TouchableOpacity
          style={{
            flexDirection: "row",
            alignItems: "center",
            alignSelf: "flex-start",
            alignContent: "center"
          }}
          transparent
          onPress={() => this.props.navigation.openDrawer()}
        >
          <Icon name="md-menu" style={{ color: "white" }} />
        </TouchableOpacity>
      </View>
    );
    return (
      <Container style={styles.container}>
        {this.renderSearchHeader()}

        {!this.props.isLoading && this.state.showSuggestion && (
          <FlatList
            style={{
              marginBottom: 5,
              borderBottomLeftRadius: 5,
              borderBottomRightRadius: 5,
              backgroundColor: Colors.transparent,
            }}
            contentContainerStyle={{backgroundColor:Colors.white}}
            data={this.state.searchSuggestions}
            renderItem={({ item }) => this.searchListItemView(item)}
            keyExtractor={item => item.type + "_" + item.id}
          />
        )}

        {!this.props.isLoading && this.renderSearch()}

        {this.props.isLoading && (
          <ActivityIndicator size="large" style={styles.activityIndicator} color={Colors.statusBarColor} />
        )}
      </Container>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  searchSuggest: (keyword, language_id) =>
    dispatch(searchSuggest(keyword, language_id)),
  search: (keyword, language_id) => dispatch(search(keyword, language_id)),
  searchVideo: (keyword, language_id, page) => dispatch(searchVideo(keyword, language_id, page)),
  searchArtist: (keyword, language_id, page) => dispatch(searchArtist(keyword, language_id, page))
});
const mapStateToProps = state => ({
  isLoading: state.serviceReducer.isLoading,
  error: state.serviceReducer.error,
  searchSuggestions: state.serviceReducer.searchSuggestions,
  searchedData: state.serviceReducer.searchedData,
  searchArtists: state.serviceReducer.searchArtists,
  searchVideos: state.serviceReducer.searchVideos,
  searchVideosPage: state.serviceReducer.searchVideosPage,
  searchArtistsPage: state.serviceReducer.searchArtistsPage,
  isSearchVideoLoading: state.serviceReducer.isSearchVideoLoading,
  isSearchRefreshVideoLoading: state.serviceReducer.isSearchRefreshVideoLoading,
  isSearchRefreshArtistLoading: state.serviceReducer.isSearchRefreshArtistLoading,
  isSearchArtistLoading: state.serviceReducer.isSearchArtistLoading,
  isSearchArtistEnd: state.serviceReducer.isSearchArtistEnd,
  isSearchVideoEnd: state.serviceReducer.isSearchVideoEnd,
  searchedData: state.serviceReducer.searchedData,
  language_id: state.serviceReducer.languageId
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);