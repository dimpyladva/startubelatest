import React, { Component } from "react";
import {
  View,
  ActivityIndicator,
  BackHandler,
  Text,
  FlatList,
  TouchableOpacity
} from "react-native";
import { CachedImage } from 'react-native-cached-image';
import { Container, Left, Right, Icon, Button, Card, Content } from "native-base";
import AppHeader from "../../../Components/AppHeader";
import { Colors } from "../../../Themes/";
import styles from "../../../Themes/styles";
import Images from "../../../Themes/Images";
import { connect } from "react-redux";
import { getRoleArtists } from "../../../Actions/ServiceCallAction";

class ArtistList extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    let artistData = params.artistData;
    this.state = {
      page: 0,
      last_page: 0,
      list: {},
      isLoading: false,
      isRefreshing: false,
      loadMore: true,
      roleId: artistData.id,
      name: artistData.name
    };
  }


  componentWillMount() {
    const { roleId } = this.state;
    if (parseInt(roleId) > 0) {
      this.loadData(roleId);
    }
  }
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }
  loadData = id => {
    const { page, last_page, isRefreshing } = this.state;
    this.setState({
      isLoading: true,
      loadMore: true
    });

    if (last_page >= page || last_page == 0) {

      this.props
        .getRoleArtists(this.props.languageId, id, this.state.page)
        .then(() => {
          const res = this.props.roleArtists;
          console.warn("ARTIST DETAILS" + res);
          if (res.status == "success") {
            this.setState({
              list:
                last_page == 0 || isRefreshing
                  ? res.artists
                  : [...this.state.list, ...res.artists],
              page: res.page,
              last_page: res.last_page,
              isRefreshing: false,
              isLoading: false,
              loadmoreLoading: false
            });
          } else {
            this.setState({
              isLoading: false,
              isRefreshing: false,
              loadmoreLoading: false
            });
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        last_page: 0,
        isRefreshing: true
      },
      () => {
        this.loadData(this.state.roleId);
      }
    );
  };

  handleLoadMore = () => {
    this.setState({ loadmoreLoading: true }, () => {
      this.loadData(this.state.roleId);
    });

  };

  renderHeader = () => {
    return (
      <View />
    );
  };
  renderFooter = () => {
    if (!this.state.isLoading || this.state.page > this.state.last_page)
      return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator color={Colors.statusBarColor} animating size="large" />
      </View>
    );
  };
  render() {
    const { isRefreshing } = this.state;
    const left = (
      <Left style={{ flex: 1 }}>
        <Button onPress={() => this.handleBackPress()} transparent>
          <Icon name="ios-arrow-back" />
        </Button>
      </Left>
    );
    const right = (
      <Right style={{ flex: 1 }}>
        <Button
          onPress={() => this.props.navigation.navigate("Home")}
          transparent
        >
          <Icon name="ios-home" />
        </Button>
        <Button
          onPress={() => this.props.navigation.navigate("Search")}
          transparent
        >
          <Icon name="ios-search" />
        </Button>
      </Right>
    );
    return (
      <Container style={styles.container}>
        <AppHeader left={left} right={right} title={this.state.name} />
        {this.state.list && this.state.list.length > 0 && (
          <FlatList
            data={this.state.list}
            horizontal={false}
            numColumns={2}
            contentContainerStyle={styles.videoContainerStyle}
            ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            keyExtractor={i => i.id}
            refreshing={isRefreshing}
            onRefresh={this.handleRefresh}
            onEndReached={
              this.state.loadMore == true ? this.handleLoadMore : null
            }
            onEndThreshold={0}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() => this.viewDetails(item)}
              >
                <View style={styles.cardViewWhiteArtist}>
                  <CachedImage
                    source={{ uri: item.image }}
                    style={styles.artistImage}
                    defaultSource={Images.defaultImageUser}
                    activityIndicatorProps={styles.imageActivityIndicator}
                  />
                  <Text style={styles.headertextSmall}>
                    {item.name}
                  </Text>
                  <Text style={styles.headertextTotalVideoSmall}>
                    {item.total_artist_video} Videos
                  </Text>
                </View>
              </TouchableOpacity>
            )}
          />
        )}
        {this.props.isLoading && !this.state.loadmoreLoading && (
          <ActivityIndicator size="large" style={styles.activityIndicator} color={Colors.statusBarColor} />
        )}
      </Container>
    );
  }
  viewDetails(data) {
    this.props.navigation.navigate("ArtistDetail", {
      artistData: data,
      title: data.name
    });
  }
  handleBackPress = () => {
    this.props.navigation.goBack(); // works best when the goBack is async
    return true;
  };
}

const mapDispatchToProps = dispatch => ({
  getRoleArtists: (languageId, artist_role_id, page) => dispatch(getRoleArtists(languageId, artist_role_id, page))
});
const mapStateToProps = state => ({
  isLoading: state.serviceReducer.isLoading,
  isActionLoading: state.serviceReducer.isActionLoading,
  error: state.serviceReducer.error,
  user: state.serviceReducer.user,
  languageId: state.serviceReducer.languageId,
  roleArtists: state.serviceReducer.roleArtists
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ArtistList);
