import React, { Component } from "react";
import {
  Image,
  StatusBar,
  View,
  TouchableOpacity,
  Platform,
  ScrollView
} from "react-native";
import {
  Text,
  Container
} from "native-base";
import Ionicons from "react-native-vector-icons/Ionicons";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
// Styles
import styles from "./styles";

import { connect } from "react-redux";

class ControlPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      langId: 1,
      activeMenu: "SignUp"
    };
  }

  render() {
    var that = this;
    const { activeMenuImage } = this.state;
    // StatusBar.setBarStyle("light-content", true);
    // if (Platform.OS === "android") {
    //   StatusBar.setBackgroundColor("transparent", true);
    //   StatusBar.setTranslucent(true);
    // }
    return (
      <Container style={styles.imgBg}>
        <View style={styles.imgContainer}>
          <View style={styles.listProfileContainer}>
            <ScrollView style={styles.menuListBg}>
              <TouchableOpacity
                onPress={() => that.props.navigation.navigate("Home")}
                style={{ flexDirection: "row", alignItems: "center" }}
              >
                <Ionicons
                  name="ios-musical-notes-outline"
                  size={30}
                  color="white"
                />
                <Text style={styles.menuListItem}>Trending Song</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.menuListItemBg}>
                <SimpleLineIcons name="playlist" size={20} color="white" />
                <Text style={styles.menuListItem}>Playlist</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.menuListItemBg}>
                <Ionicons
                  name="ios-musical-notes-outline"
                  size={30}
                  color="white"
                />
                <Text style={styles.menuListItem}>Library</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.menuListItemBg}>
                <Ionicons
                  name="ios-musical-notes-outline"
                  size={30}
                  color="white"
                />
                <Text style={styles.menuListItem}>Radio</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.menuListItemBg}>
                <Ionicons name="ios-albums-outline" size={30} color="white" />
                <Text style={styles.menuListItem}>Feed</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.menuListItemBg}>
                <Ionicons
                  name="ios-musical-notes-outline"
                  size={30}
                  color="white"
                />
                <Text style={styles.menuListItem}>My Likes</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.menuListItemBg}>
                <SimpleLineIcons name="settings" size={25} color="white" />
                <Text style={styles.menuListItem}>Settings</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
      </Container>
    );
  }
}
const mapStateToProps = state => {
  return {
    // ...redux state to props here
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ControlPanel);
