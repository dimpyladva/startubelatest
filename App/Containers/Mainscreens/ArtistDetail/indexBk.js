import React, { Component } from "react";
import { connect } from "react-redux";
import { getArtistDetails } from '../../../Actions/ServiceCallAction';
import {
  View,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Text,
  BackHandler,
  Image,
  ScrollView,
  Animated,
  Dimensions
} from "react-native";
import {
  Container,
  Left,
  Right,
  Icon,
  Button
} from "native-base";
import VideoCardItem from "../../../Components/VideoCardItem";
import { ListItem, SearchBar } from "react-native-elements"; // 0.19.1
import AppHeader from "../../../Components/AppHeader";
import { Colors, Fonts } from "../../../Themes";
import styles from '../../../Themes/styles';
import { Images } from "../../../Themes/";
const HEADER_MIN_HEIGHT = 55;
const HEADER_MAX_HEIGHT = 180;
const HEADER_EXPANDED_HEIGHT = 180;
const HEADER_COLLAPSED_HEIGHT = 55;

const { width: SCREEN_WIDTH } = Dimensions.get("screen")

class ArtistDetail extends Component {

  constructor() {
    super();

    this.scrollYAnimatedValue = new Animated.Value(0);

    this.array = [];
  }
  state = {
    seed: 1,
    page: 1,
    last_page: 0,
    list: {},
    isLoading: false,
    isRefreshing: false,
    showTheSearchbar: false,
    loadMore: true,
    artistId: null,
    type: null,
    artistData: {},
    input: "",
    scrollY: new Animated.Value(0),
    totalVideos: 0,
  };
  componentWillMount() {
    const { params } = this.props.navigation.state;
    var artistData = params.artistData;
    // console.warn(params.artistData);
    this.setState({ artistId: 2, artistData: artistData })
    this.loadData(2);

  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }
  animatedView() {

  }
  render() {
    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
      outputRange: [HEADER_EXPANDED_HEIGHT, HEADER_COLLAPSED_HEIGHT],
      extrapolate: 'clamp'
    });
    const headerTitleOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
      outputRange: [0, 1],
      extrapolate: 'clamp'
    });
    const heroTitleOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
      outputRange: [1, 0],
      extrapolate: 'clamp'
    });
    var that = this;
    const { list, isRefreshing } = this.state;
    // const headerHeight = this.scrollYAnimatedValue.interpolate(
    //   {
    //     inputRange: [0, (HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT)],
    //     outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
    //     extrapolate: 'clamp'
    //   });

    const headerBackgroundColor = this.scrollYAnimatedValue.interpolate(
      {
        inputRange: [0, (HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT)],
        outputRange: [Colors.navbarBackgroundColor, '#01579B'],
        extrapolate: 'clamp'
      });

    var left = (
      <Left style={{ flex: 1 }}>
        <Button onPress={() => this.handleBackPress()} transparent>
          <Icon name="ios-arrow-back" />
        </Button>
      </Left>
    );
    var right = (
      <Right style={{ flex: 1 }}>
        <Button onPress={() => this.setState({ showTheSearchbar: true })} transparent>
          <Icon name="ios-search" />
        </Button>

      </Right>
    );
    return (
      <Container>
        <AppHeader left={left} right={right} title={this.state.artistData.artist_name}
        />
        <View style={styles.container}>
          {/* <Animated.View style={[styles.header, { height: headerHeight }]}>
            <Animated.Text style={{ textAlign: 'center', fontSize: 18, backgroundColor: Colors.navbarBackgroundColor, color: 'white', marginTop: 28, opacity: headerTitleOpacity }}>Total Videos</Animated.Text>
            <Animated.Text style={{ textAlign: 'center', fontSize: 32, backgroundColor: Colors.navbarBackgroundColor, color: 'white', position: 'absolute', bottom: 20, left: 16, opacity: heroTitleOpacity }}>MAin</Animated.Text>
            <Animated.Image
              source={{
                uri: this.state.artistData.image
              }}
              defaultSource={Images.defaultImage}
              loadingIndicatorSource={Images.defaultImage}
              style={{ width: '100%', height: '100%', resizeMode: 'cover', opacity: heroTitleOpacity }}>
            </Animated.Image>
          </Animated.View> */}



          <Animated.View style={{ backgroundColor: Colors.navbarBackgroundColor }}>
            <Animated.Image
              style={{
                height: headerHeight,
                width: '100%',
                resizeMode: "cover",
                opacity: heroTitleOpacity
              }}
              source={{ uri: this.state.artistData.image }}
            />
            <Animated.View style={{
              alignItems: "center",
              justifyContent: "center",
              flexDirection: 'row',
              bottom: 0,
              width: '100%',
              position: 'absolute',
              backgroundColor: Colors.transparentBlack,
              opacity: heroTitleOpacity,
              padding: 5
            }}>
              <Icon name="ios-videocam" style={{ fontSize: 22, color: 'white', }} />
              <Animated.Text style={{ marginLeft: 8, fontFamily: Fonts.type.sfuiDisplaySemibold, textAlign: 'center', fontSize: 16, backgroundColor: 'transparent', color: 'white' }}>Videos: {this.state.totalVideos}</Animated.Text>
            </Animated.View>

            <Animated.Text style={{ opacity: headerTitleOpacity, left: 80, fontFamily: Fonts.type.sfuiDisplaySemibold, textAlign: 'center', fontSize: 15, backgroundColor: Colors.navbarBackgroundColor, color: 'white', textAlign: 'center', bottom: 18, position: 'absolute' }}>Total Videos: {this.state.totalVideos}</Animated.Text>
            <Animated.Image
              source={{ uri: this.state.artistData.image }}
              defaultSource={Images.defaultImage}
              loadingIndicatorSource={Images.defaultImage}
              style={{
                width: 50,
                height: 50,
                borderRadius: 25,
                margin: 5,
                left: '5%',
                resizeMode: "cover",
                backgroundColor: 'transparent',
                bottom: 0, position: 'absolute',
                opacity: headerTitleOpacity,
              }}
            />

          </Animated.View>



          <ScrollView
            contentContainerStyle={styles.scrollContainer}
            onScroll={Animated.event(
              [{
                nativeEvent: {
                  contentOffset: {
                    y: this.state.scrollY
                  }
                }
              }])
            }
            scrollEventThrottle={16}>
            {list && (
              <FlatList
                horizontal={false}
                numColumns={2}
                data={list}
                renderItem={({ item: rowData }) => (
                  <VideoCardItem
                    data={rowData}
                    {...this.props}
                  />
                )}
                ListHeaderComponent={this.renderHeader}
                ListFooterComponent={this.renderFooter}
                keyExtractor={i => i.email}
                refreshing={isRefreshing}
                onRefresh={this.handleRefresh}
                onEndReached={
                  this.state.loadMore == true ? this.handleLoadMore : null
                }
                onEndThreshold={0}
              />
            )}
          </ScrollView>
          {/* <ScrollView
            contentContainerStyle={{ paddingTop: HEADER_MAX_HEIGHT }}
            scrollEventThrottle={16}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.scrollYAnimatedValue } } }]
            )}>


          </ScrollView> */}


        </View>

      </Container >




    );
  }
  //   <Animated.View style={[styles.animatedHeader, { height: headerHeight, backgroundColor: headerBackgroundColor }]}>
  //   <View style={{ backgroundColor: Colors.bloodOrange, width: '100%' }}>

  //     {/* <Image
  //       source={{
  //         uri: this.state.artistData.image
  //       }}
  //       defaultSource={Images.defaultImage}
  //       loadingIndicatorSource={Images.defaultImage}
  //       style={{ width: '100%', height: '100%', resizeMode: 'cover' }}>
  //     </Image>
  //     <View style={styles.viewOverlay}>
  //     <View style={styles.homeCategoryBorder} />
  //       <Text style={styles.detailPageTextTitle}>Total Videos</Text>
  //       <Text style={styles.detailPageTextTitle}>10</Text>
  //     </View>
  //     <View> */}
  //     <Image
  //       style={{ width: '100%', height: '100%', resizeMode: 'cover' }}
  //       source={{
  //         uri: this.state.artistData.image
  //       }}
  //     />
  //     <View style={styles.viewOverlay} />
  //     <View style={styles.viewBorder}>
  //       <View style={styles.homeCategoryText}>
  //         <Text style={styles.detailPageTextTitle}>Total Videos</Text>
  //         <Text style={styles.detailPageTextTitle}>10</Text>
  //       </View>
  //     </View>
  //   </View>
  // </Animated.View>
  loadData = (id) => {
    const { page, last_page, isRefreshing } = this.state;
    this.setState({
      isLoading: true,
      loadMore: true,
    });

    if (last_page >= page || last_page == 0) {

      // fetch(
      //   //'http://share.netispy.com/star_tube/api/v1/categories/videos?type=new&category_id=6&limit=5&page=${last_page}
      //   `http://share.netispy.com/star_tube/api/v1/categories/videos?type=new&category_id=6&limit=5&page=${page}`
      // )
      // .then(res => res.json())
      this.props.getArtistDetails(id, this.state.page)
        .then(() => {
          let res = this.props.artistDetail;
          if (res.status == 'success') {

            this.setState({
              list:
                last_page == 0 || isRefreshing ? res.videos : [...this.state.list, ...res.videos],
              page: res.page,
              last_page: res.last_page,
              isRefreshing: false,
              isLoading: false,
              totalVideos: res.total_videos
            });
          } else {
            this.setState({ isLoading: false });
          }
        })
        .catch(err => {
          console.error(err);
        });
    }
  };



  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        last_page: 0,
        isRefreshing: true
      },
      () => {
        this.loadData(this.state.artistId);
      }
    );
  };

  handleLoadMore = () => {
    this.loadData(this.state.artistId);
  };

  handleBackPress = () => {
    this.props.navigation.goBack(); // works best when the goBack is async
    return true;
  }

  renderHeader = () => {
    return (
      <View></View>
      // <View style={{ backgroundColor: Colors.navbarBackgroundColor }}>
      //   <Text style={styles.detailPageTextTitle}>Total Videos</Text>
      //   <Text style={styles.detailPageTextTitle}>10</Text>
      // </View>

    );
  };
  /* {this.state.showTheSearchbar &&
         <SearchBar
           onChangeText={text => this.searchData(text)}
           placeholder="Type Here..."
           lightTheme
           round
         />
       } */
  // <Header searchBar rounded>
  //   <Item>
  //     <Icon name="ios-search" />
  //     <Input placeholder="Search" />
  //     <Icon name="ios-people" />
  //   </Item>
  //   <Button transparent>
  //     <Text>Search</Text>
  //   </Button>
  // </Header>
  renderFooter = () => {
    // console.warn(this.state.page + " last:" + this.state.last_page);

    if (!this.state.isLoading || this.state.page > this.state.last_page) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
            <ActivityIndicator size="large" style={styles.activityIndicator} color={Colors.statusBarColor} />
      </View>
    );
  };

}

const s = StyleSheet.create({
  scene: {
    flex: 1,

  },
  list: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  scrollContainer: {
    padding: 16,
    paddingTop: HEADER_EXPANDED_HEIGHT
  },
});

const mapDispatchToProps = (dispatch) => ({
  getArtistDetails: (artist_id, page) => dispatch(getArtistDetails(artist_id, page)),
});
const mapStateToProps = (state) => ({
  isLoading: state.serviceReducer.isLoading,
  error: state.serviceReducer.error,
  artistDetail: state.serviceReducer.artistDetail
});
export default connect(mapStateToProps, mapDispatchToProps)(ArtistDetail);