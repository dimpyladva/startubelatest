import React, { Component } from "react";
import { connect } from "react-redux";
import { getArtistDetails } from "../../../Actions/ServiceCallAction";
import {
  View,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Text,
  BackHandler,
  Image,
  ScrollView,
  Animated
} from "react-native";
import { CachedImage } from "react-native-cached-image";
import { Container, Left, Right, Icon, Button, Content } from "native-base";
import VideoCardItem from "../../../Components/VideoCardItem";
import AppHeader from "../../../Components/AppHeader";
import { Colors, Fonts } from "../../../Themes";
import styles from "../../../Themes/styles";
import { Images, Metrics } from "../../../Themes/";

class ArtistDetail extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    var artistData = params.artistData;
    var title = params.title;
    this.array = [];
    this.state = {
      seed: 1,
      page: 1,
      last_page: 0,
      list: {},
      isLoading: false,
      isRefreshing: false,
      showTheSearchbar: false,
      loadMore: true,
      artistId: artistData.id,
      type: null,
      artistData: artistData,
      input: "",
      image: artistData.image,
      totalVideos: 0,
      title
    };
  }

  componentWillMount() {
    const { artistId } = this.state;
    if (parseInt(artistId) > 0) {
      this.loadData(artistId);
    }
  }
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  render() {
    var that = this;
    const { isRefreshing } = this.state;
    var left = (
      <Left style={{ flex: 1 }}>
        <Button onPress={() => this.handleBackPress()} transparent>
          <Icon name="ios-arrow-back" />
        </Button>
      </Left>
    );
    var right = (
      <Right style={{ flex: 1 }}>
        <Button
          onPress={() => this.props.navigation.navigate("Home")}
          transparent
        >
          <Icon name="ios-home" />
        </Button>
        <Button
          onPress={() => this.setState({ showTheSearchbar: true })}
          transparent
        >
          <Icon name="ios-search" />
        </Button>
      </Right>
    );
    return (
      <Container style={styles.container}>
        <AppHeader left={left} right={right} title={this.state.title} />

        <View style={styles.container}>
          <Content>
            <View style={{ backgroundColor: Colors.navbarBackgroundColor }}>
              <CachedImage
                style={styles.artistImageDetail}
                defaultSource={Images.defaultImage}
                source={{ uri: this.state.image }}
                activityIndicatorProps={styles.imageActivityIndicator}
              />
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "row",
                  padding: Metrics.WIDTH * 0.02
                }}
              >
                <Icon
                  name="ios-videocam"
                  style={{ fontSize: 22, color: "white" }}
                />
                <Text
                  style={{
                    marginLeft: 8,
                    fontFamily: Fonts.type.sfuiDisplaySemibold,
                    textAlign: "center",
                    fontSize: 16,
                    backgroundColor: "transparent",
                    color: "white"
                  }}
                >
                  Videos: {this.state.totalVideos}
                </Text>
              </View>
            </View>
            {this.state.isLoading && !this.state.loadmoreLoading && (
              <ActivityIndicator
                size="large"
                style={styles.activityIndicator}
                color={Colors.statusBarColor}
              />
            )}
            {this.state.list && this.state.list.length > 0 ? (
              <View style={styles.scrollContainer}>
                <FlatList
                  horizontal={false}
                  numColumns={2}
                  contentContainerStyle={styles.videoContainerStyle}
                  data={this.state.list}
                  renderItem={({ item: rowData }) => (
                    <VideoCardItem
                      isSmall={true}
                      data={rowData}
                      {...this.props}
                    />
                  )}
                  ListHeaderComponent={this.renderHeader}
                  ListFooterComponent={this.renderFooter}
                  keyExtractor={i => i.id}
                  refreshing={isRefreshing}
                  onRefresh={this.handleRefresh}
                  onEndReached={
                    this.state.loadMore == true ? this.handleLoadMore : null
                  }
                  onEndThreshold={0}
                />
              </View>
            ) : null}
          </Content>
        </View>
      </Container>
    );
  }

  loadData = id => {
    const { page, last_page, isRefreshing } = this.state;
    this.setState({
      isLoading: true,
      loadMore: true
    });

    if (last_page >= page || last_page == 0) {
      this.props
        .getArtistDetails(this.props.languageId, id, this.state.page)
        .then(() => {
          const res = this.props.artistDetail;
          // console.warn("ARTIST DETAILS" + res);
          if (res.status == "success") {
            this.setState({
              list:
                last_page == 0 || isRefreshing
                  ? res.videos
                  : [...this.state.list, ...res.videos],
              page: res.page,
              last_page: res.last_page,
              isRefreshing: false,
              isLoading: false,
              totalVideos: res.total_videos,
              loadmoreLoading: false,
              image: res.image
            });
          } else {
            this.setState({
              isLoading: false,
              isRefreshing: false,
              loadmoreLoading: false
            });
          }
        })
        .catch(err => {
          console.warn(err);
        });
    }
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        last_page: 0,
        isRefreshing: true
      },
      () => {
        this.loadData(this.state.artistId);
      }
    );
  };

  handleLoadMore = () => {
    this.setState({ loadmoreLoading: true });
    this.loadData(this.state.artistId);
  };

  handleBackPress = () => {
    this.props.navigation.goBack(); // works best when the goBack is async
    return true;
  };

  renderHeader = () => {
    return <View />;
  };
  renderFooter = () => {
    if (!this.state.isLoading || this.state.page > this.state.last_page)
      return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator
          color={Colors.statusBarColor}
          animating
          size="large"
        />
      </View>
    );
  };
}

const mapDispatchToProps = dispatch => ({
  getArtistDetails: (languageId, artist_id, page) =>
    dispatch(getArtistDetails(languageId, artist_id, page))
});
const mapStateToProps = state => ({
  isLoading: state.serviceReducer.isLoading,
  languageId: state.serviceReducer.languageId,
  error: state.serviceReducer.error,

  artistDetail: state.serviceReducer.artistDetail
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ArtistDetail);
