import React, { Component } from "react";
import { ScrollView, ActivityIndicator, BackHandler } from "react-native";
import HTML from "react-native-render-html";
import {
  Container,
  Left,
  Right,
  Icon,
  Button,
  Content,
  Text,
  View,
  Card
} from "native-base";
import AppHeader from "../../../Components/AppHeader";
import { Colors, Fonts, Metrics, ApplicationStyles } from "../../../Themes/";
import styles from "../../../Themes/styles";
import { connect } from "react-redux";
import { getGeneralDetails } from "../../../Actions/ServiceCallAction";

class Contact extends Component {
  componentDidMount() {
    this.props.getGeneralDetails();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(); // works best when the goBack is async
    return true;
  };
  render() {
    var left = (
      <Left style={{ flex: 1 }}>
        <Button transparent onPress={() => this.props.navigation.openDrawer()}>
          <Icon name="md-menu" />
        </Button>
      </Left>
    );
    var right = (
      <Right style={{ flex: 1 }}>
        <Button
          onPress={() => this.props.navigation.navigate("Home")}
          transparent
        >
          <Icon name="ios-home" />
        </Button>
        <Button
          onPress={() => this.props.navigation.navigate("Search")}
          transparent
        >
          <Icon name="ios-search" />
        </Button>
      </Right>
    );
    const { generalDetails } = this.props;
    return (
      <Container style={styles.container}>
        <AppHeader left={left} right={right} title="Contact Us" />
        {generalDetails && generalDetails.details && (
          <Content padder>
            <ScrollView
              style={{
                flex: 1
              }}
            >
              <Card style={styles.cardStyle}>
                <Text style={styles.contactHeader}>Email Address</Text>
                <HTML
                  html={generalDetails.details.email_address}
                  imagesMaxWidth={Metrics.WIDTH}
                  baseFontStyle={{ fontSize: Fonts.moderateScale(15) }}
                />
              </Card>
              <Card style={styles.cardStyle}>
                <Text style={styles.contactHeader}>Customer Support</Text>
                <HTML
                  html={generalDetails.details.customer_support}
                  imagesMaxWidth={Metrics.WIDTH}
                  baseFontStyle={{ fontSize: Fonts.moderateScale(15) }}
                />
              </Card>
              <Card style={styles.cardStyle}>
                <Text style={styles.contactHeader}>Office Address</Text>
                <HTML
                  html={generalDetails.details.office_address}
                  imagesMaxWidth={Metrics.WIDTH}
                  baseFontStyle={{ fontSize: Fonts.moderateScale(15) }}
                />
              </Card>
            </ScrollView>
          </Content>
        )}
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getGeneralDetails: () => dispatch(getGeneralDetails())
});
const mapStateToProps = state => ({
  isLoading: state.serviceReducer.isLoading,
  generalDetails: state.serviceReducer.generalDetails
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Contact);