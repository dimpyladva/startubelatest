import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ActivityIndicator,
  BackHandler,
  Text,
  FlatList,
  TouchableOpacity
} from "react-native";
import { CachedImage } from 'react-native-cached-image';
import {
  Container,
  Left,
  Right,
  Icon,
  Button,
  Spinner,
  Content,
  Card,
  CardItem
} from "native-base";
import AppHeader from "../../../Components/AppHeader";
import { Colors, Images, Metrics } from "../../../Themes/";
import styles from "../../../Themes/styles";
import { connect } from "react-redux";
import { getArtists } from "../../../Actions/ServiceCallAction";

class Artists extends Component {
  state = {
    seed: 1,
    page: 0,
    nextPage: 0,
    isLoading: false,
    isRefreshing: false,
    loadMore: true,
    input: "",
    showTheSearchBar: false,
    favIcon: 0,
    watchIcon: 0,
    videoItem: null
  };

  componentWillMount() {
    this.props.getArtists(this.props.languageId);
  }
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(); // works best when the goBack is async
    return true;
  };
  render() {
    const { isRefreshing } = this.state;
    // var left = (
    //   <Left style={{ flex: 1 }}>
    //     <Button onPress={() => this.handleBackPress()} transparent>
    //       <Icon name="ios-arrow-back" />
    //     </Button>
    //   </Left>
    // );
    var left = (
      <Left style={{ flex: 1 }}>
        <Button
          transparent
          onPress={() => this.props.navigation.openDrawer()}
        >
          <Icon name="md-menu" />
        </Button>
      </Left>
    );
    var right = (
      <Right style={{ flex: 1 }}>
        <Button
          onPress={() => this.props.navigation.navigate("Home")}
          transparent
        >
          <Icon name="ios-home" />
        </Button>
        <Button
          onPress={() => this.props.navigation.navigate("Search")}
          transparent
        >
          <Icon name="ios-search" />
        </Button>
      </Right>
    );

    return (
      <Container style={styles.container}>
        <AppHeader left={left} right={right} title="Artists" />
        {!this.props.isLoading &&
          <Content padder>

            <FlatList
              data={this.props.artists}
              showsHorizontalScrollIndicator={false}
              renderItem={({ item }) => (
                item.artists.length > 0 &&
                <View style={styles.cardViewWhite}>
                  <View style={{ flexDirection: "column", width: Metrics.WIDTH * 0.90 }}>
                    <View style={styles.connectionHeaderBg}>
                      <View style={{ flexDirection: "row" }}>
                        <View style={styles.verticalLine} />
                        <Text style={styles.connectionPhotosTxt}>
                          {item.name}
                        </Text>
                      </View>
                      <TouchableOpacity
                        onPress={() => this.viewDetails(item)}
                        style={styles.connectionPhotoCountBg}
                      >
                        <Text style={styles.connectionPhotosCountTxt}>
                          {this.props.languageConstants.LBL_VIEW_ALL}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={styles.suggestedPeopleImgView}>
                      {this.renderArtists(item.artists)}
                    </View>
                  </View>
                </View>

              )}
            />
          </Content>
        }
        {this.props.isLoading && (
          <ActivityIndicator size="large" style={styles.activityIndicator} color={Colors.statusBarColor} />
        )}

      </Container>
    );
  }
  viewDetails(data) {
    this.props.navigation.navigate("ArtistList", { artistData: data });
  }
  renderArtists(data) {
    return data.map((art, idx) => {
      if (idx == 0) {
        return (
          <View style={styles.sugestedPeopleImgOne}>
            <CachedImage
              source={{ uri: art.image }}
              style={styles.sugestedPeopleImageOne}
              defaultSource={Images.defaultImageUser}
              activityIndicatorProps={styles.imageActivityIndicator}
            />
          </View>
        );
      } else {
        return (
          <View style={styles.sugestedPeopleImgTwo}>
            <CachedImage
              style={styles.sugestedPeopleImageTwo}
              source={{ uri: art.image }}
              defaultSource={Images.defaultImageUser}
              activityIndicatorProps={styles.imageActivityIndicator}
            />
          </View>

        );
      }
    });
  }
}

const mapDispatchToProps = dispatch => ({
  getArtists: (languageId) => dispatch(getArtists(languageId))
});
const mapStateToProps = state => ({
  isLoading: state.serviceReducer.isLoading,
  isActionLoading: state.serviceReducer.isActionLoading,
  languageConstants: state.serviceReducer.languageConstants,
  error: state.serviceReducer.error,
  user: state.serviceReducer.user,
  languageId: state.serviceReducer.languageId,
  artists: state.serviceReducer.artists
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Artists);

// renderHeader = () => {
//   return (
//     <SearchBar
//       onChangeText={text => this.searchData(text)}
//       placeholder="Type Here..."
//       round
//     />
//   );
// };
// renderFooter = () => {
//   if (!this.state.isLoading) return null;
//   return (
//     <View
//       style={{
//         paddingVertical: 20,
//         borderTopWidth: 1,
//         borderColor: "#CED0CE"
//       }}
//     >
//       <ActivityIndicator animating size="large" />
//     </View>
//   );
// };

// loadUsers = () => {
//   const { users, seed, page, nextPage } = this.state;

//   this.setState({
//     isLoading: true,
//     loadMore: true,
//     page: this.state.nextPage
//   });

//   if (page < nextPage || nextPage == 0) {
//     fetch(
//       `https://monkeywebstudio.com/itv3/application/index.php?r=api/employee-list&page=${nextPage}`
//     )
//       .then(res => res.json())
//       .then(res => {
//         if (res.status) {
//           this.setState({
//             users:
//               nextPage == 0 ? res.data : [...this.state.users, ...res.data],
//             nextPage: res.next_page,
//             isRefreshing: false
//           });
//         } else {
//           this.setState({ isLoading: false });
//         }
//       })
//       .catch(err => {
//         console.error(err);
//       });
//   }
// };

// searchData = text => {
//   this.setState({
//     isLoading: true,
//     loadMore: false,
//     input: text,
//     page: 0,
//     nextPage: 0,
//     users: []
//   });

//   let input = this.state.input;
//   if (text == "") {
//     this.loadUsers();
//   } else {
//     fetch(
//       `https://monkeywebstudio.com/itv3/application/index.php?r=api/employee-list&name=${text}`
//     )
//       .then(res => res.json())
//       .then(res => {
//         if (res.status) {
//           this.setState({
//             users: res.data,
//             isLoading: false
//           });
//         } else {
//           this.setState({ isLoading: false });
//         }
//       })
//       .catch(err => {
//         console.error(err);
//       });
//   }
// };

// handleRefresh = () => {
//   this.setState(
//     {
//       nextPage: 0,
//       isRefreshing: true
//     },
//     () => {
//       this.loadUsers();
//     }
//   );
// };

// handleLoadMore = () => {
//   this.loadUsers();
// };

// componentDidMount() {
//   this.loadUsers();
// }
