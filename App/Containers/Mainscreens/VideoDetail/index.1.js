import React, { Component } from "react";
import {
  View,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Alert,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  Share,
  BackHandler,
  ImageBackground,
  PixelRatio
} from "react-native";

import {
  Container,
  Left,
  Right,
  Icon,
  Button,
  Spinner,
  Content,
  Col
} from "native-base";
import Orientation from 'react-native-orientation';

import VideoCardItem from "../../../Components/VideoCardItem";
import AppHeader from "../../../Components/AppHeader";
import YouTube from "react-native-youtube";
import { Colors, Metrics } from "../../../Themes/";
import styles from "../../../Themes/styles";
import string from "../../../Themes/String";
import { connect } from "react-redux";
import { videoDetail } from "../../../Actions/ServiceCallAction";
import { addLikeRequest } from "../../../Actions/ServiceCallAction";
import { addWatchLaterRequest } from "../../../Actions/ServiceCallAction";
import ArtistView from "../../../Components/ArtistView";
var languageId, videoId, userId;
//import Orientation from 'react-native-orientation'
class DetailPage extends Component {
  constructor(props){
    super(props);
    this.state = {
      favIcon: 0,
      watchIcon: 0,
      videoItem: null,
      landscape: false,
      showVideo: false,
      isReady: false,
      status: "",
      quality: 0,
      error: "",
      containerMounted: false,
      videoHeight: Metrics.HEIGHT*0.35,
      currentOrientation:Orientation.getInitialOrientation()
    };
    this._orientationDidChange = this._orientationDidChange.bind(this);

  }
  componentWillMount() {
    const { params } = this.props.navigation.state;
    this.setState({ videoItem: params.videoItem });

    userId = this.props.user.user_id;
    languageId = this.props.language_id;
    videoId = params.videoItem.id;
    console.warn("userId: " + userId + "languageId: " + languageId + "videoId: " + videoId);
    this.props.videoDetail(languageId, videoId, userId, false);
  }
  componentDidMount() {
    Orientation.unlockAllOrientations()
    Orientation.addOrientationListener(this._orientationDidChange)
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);

  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.refreshVideoDetail != this.props.refreshVideoDetail && this.props.refreshVideoDetail) {
      this.props.videoDetail(languageId, videoId, userId, true);
    }
    // console.log("componentDidUpdate")
    // console.log(prevProps)
    // console.log(this.props)
    // if(this.state.currentOrientation!=this.props.currentOrientation){
      
    //   setTimeout(() => {
    //     this.setState({ 
    //       currentOrientation:this.props.currentOrientation,
    //       videoHeight: this.props.currentOrientation == "LANDSCAPE" ? (Metrics.HEIGHT * 0.35 + 9):(Metrics.HEIGHT * 0.35 + 7)
    //     });
    //   })
    // }

  }
  componentWillUnmount() {
    Orientation.lockToPortrait();
    Orientation.removeOrientationListener(this._orientationDidChange)
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }
  _orientationDidChange(orientation) {
    console.log("change oriention")
    // setTimeout(() => {
      console.log("change oriention timeout")
      this.setState({ 
        currentOrientation:orientation,
        videoHeight: orientation == "LANDSCAPE" ? (Metrics.HEIGHT * 0.35 + 9):(Metrics.HEIGHT * 0.35 + 7)
      });
    // },500)
  }
  handleBackPress = () => {
    this.props.navigation.goBack(); // works best when the goBack is async
    return true;
  };
  render() {
    var details = this.props.videoDetailData &&
      this.props.videoDetailData.video_details &&
      this.props.videoDetailData.video_details.id == videoId ? this.props.videoDetailData.video_details : null;
    //var details = this.props.videoDetailData.video_details ;
    const { isRefreshing } = this.state;
    var left = (
      <Left style={{ flex: 1 }}>
        <Button onPress={() => this.handleBackPress()} transparent>
          <Icon name="ios-arrow-back" />
        </Button>
      </Left>
    );
    var right = (
      <Right style={{ flex: 1 }}>
        <Button
          onPress={() => this.props.navigation.navigate("Home")}
          transparent
        >
          <Icon name="ios-home" />
        </Button>
        <Button
          onPress={() => this.props.navigation.navigate("Search")}
          transparent
        >
          <Icon name="ios-search" />
        </Button>
      </Right>
    );
    return (
      <Container style={styles.container}>
        <AppHeader left={left} right={right} title="Startube" />
        {!this.props.isLoading && details && (
          <ScrollView
            onLayout={({ nativeEvent: { layout: { width } } }) => {
              if (!this.state.containerMounted) this.setState({ containerMounted: true });
              if (this.state.containerWidth !== width) this.setState({ containerWidth: width });
            }}>
            {this.renderYoutubePlayer(details)}
            {this.renderArtistInfo(this.props.videoDetailData.artists)}
            {this.renderMoreVideo(this.props.videoDetailData.more_videos)}
          </ScrollView>
        )}
        {(this.props.isLoading || details == null) && (
          <ActivityIndicator size="large" style={styles.activityIndicator} color={Colors.statusBarColor} />
        )}
      </Container>
    );
  }
  renderNameWithActionButtons(details) {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignContent: "center",
          alignItems: "center",
          backgroundColor: Colors.white,
          paddingHorizontal: Metrics.WIDTH * 0.03,
        }}
      >
        <View style={{ width: Metrics.WIDTH * 0.70 }}>
          <Text style={styles.detailNameTitle}>{details.title}</Text>
        </View>
        <View
          style={{
            flex: 1,
            width: Metrics.WIDTH * 0.30,
            flexDirection: "row",
            justifyContent: 'flex-end',
          }}
        >
          {this.props.isActionLoading && (
            <ActivityIndicator size="small" color={Colors.primaryColor} />
          )}
          <Button onPress={() => this.addLike()} transparent>
            {this.props.videoDetailData.user_liked ? (
              <Icon name="ios-heart" style={styles.smallIconColored} />
            ) : (
                <Icon name="ios-heart" style={styles.smallIcon} />
              )}
          </Button>
          <Button onPress={() => this.addViewLater()} transparent>
            {this.props.videoDetailData.user_view_later ? (
              <Icon name="ios-time" style={styles.smallIconColored} />
            ) : (
                <Icon name="ios-time" style={styles.smallIcon} />
              )}
          </Button>
          <Button onPress={() => this.share()} transparent>
            <Icon name="md-share" style={styles.smallIcon} />
          </Button>
        </View>
      </View>
    );
  }
  share() {
    var details = this.props.videoDetailData;
    Share.share(
      {
        message: details.video_details.title,
        url: details.video_details.url,
        title: string.AppName
      },
      {
        // Android only:
        dialogTitle: "Share Video",
        // iOS only:
        excludedActivityTypes: ["com.apple.UIKit.activity.PostToTwitter"]
      }
    );
  }
  renderYoutubePlayer(vDetails) {
   console.log("this.props.currentOrientation ")
   console.log(this.state.currentOrientation );
   console.log(this.state.videoHeight)


    return (
      <View style={{ flexDirection: "column" }}>
        {this.state.showVideo ? (
          <YouTube
            hidden={false}
            videoId={vDetails.video_id} // The YouTube video ID
            play={true} // control playback of video with true/false
            playsInline={true}     // control whether the video should play inline
            apiKey="AIzaSyDobd_4OKUfDaazchfZXyu_H93U8Y_MEEc"
            //playsInline={this.props.currentOrientation != "LANDSCAPE"} // control whether the video should play inline
            fullscreen={this.state.currentOrientation == "LANDSCAPE"?true:false}
            onFullScreenEnter={() => Orientation.unlockAllOrientations()}
            // onFullScreenExit={() => {
            //   setTimeout(() => {
            //     //Orientation.lockToPortrait()
            //   },500)
            // }}
              
            controls={1}
            loop={false}
            onReady={e => {
              this.setState({ isReady: true,
                videoHeight:(Metrics.HEIGHT * 0.35 + 10) 
              });
            }}
            onChangeState={e => {
              //this.setState({ status: e.state});
            }}
            onChangeQuality={e => {
              //this.setState({ quality: e.quality });
            }}
            onError={e => {
              console.log("onError")

              console.log(e)
              //this.setState({ error: e.error });
            }}
            style={{
              height:this.state.videoHeight,
              alignSelf: 'stretch', 
            }}
          />
        ) : (
            <ImageBackground
              style={[styles.videoCatalougeImage, { height: this.state.videoHeight }]}
              source={{ uri: vDetails.thumbnail }}
            >
              <Button
                transparent
                style={styles.playIconContainer}
                onPress={() => {
                  Orientation.unlockAllOrientations()
                  this.setState({ showVideo: true })
                }}
              >
                <Icon style={{ fontSize: 50, color: "white" }} name="md-play" />
              </Button>
            </ImageBackground>
          )}
        {/* <YouTube
          videoId={vDetails.video_id} // The YouTube video ID
          play={true} // control playback of video with true/false
          hidden={false} // control visiblity of the entire view
          playsInline={true} // control whether the video should play inline
          apiKey="AIzaSyDobd_4OKUfDaazchfZXyu_H93U8Y_MEEc"
          // onReady={e => {
          //   this.setState({ isReady: true });
          // }}
          // onChangeState={e => {
          //   this.setState({ status: e.state });
          // }}
          // onChangeQuality={e => {
          //   this.setState({ quality: e.quality });
          // }}
          // onError={e => {
          //   this.setState({ error: e.error });
          // }}
          style={{
            alignSelf: "stretch",
            height: 300,
            backgroundColor: "black",
            marginVertical: 10
          }}
        /> */}
        {this.renderNameWithActionButtons(vDetails)}
      </View>
    );
  }
  artistDetailPage() { }
  renderArtistInfo(artists) {
    return (
      <View>
        {artists && (
          <View style={styles.detailPageBg}>
            <View style={styles.connectionHeaderBg}>
              <Text style={styles.connectionPhotosTxt}>Information</Text>
              <TouchableOpacity
                style={styles.connectionPhotoCountBg}
                onPress={() => alert("Artists")}
              />
            </View>
            <View style={styles.connectionProfileBg}>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={artists}
                renderItem={({ item: rowData }) => (
                  <ArtistView
                    data={rowData}
                    navigation={this.props.navigation}
                  />
                )}
                keyExtractor={(item, index) => index}
              />
            </View>
          </View>
        )}
      </View>
    );
  }
  renderMoreVideo(morevideos) {
    return (
      <View>
        {morevideos.length > 0 && (
          <View style={styles.connectionBg}>
            <View style={styles.connectionHeaderBg}>
              <Text style={styles.connectionPhotosTxt}>Like Videos</Text>
              <TouchableOpacity
                style={styles.connectionPhotoCountBg}
              // onPress={() => alert("Artists")}
              />
            </View>
            <View style={styles.connectionProfileBg}>
              <FlatList
                horizontal
                data={morevideos}
                renderItem={({ item: rowData }) => {
                  return (
                    <VideoCardItem
                      data={rowData}
                      navigation={this.props.navigation}
                    />
                  );
                }}
                keyExtractor={(item, index) => index}
              />
            </View>
          </View>
        )}
      </View>
    );
  }
  addLike() {
    this.props.addLikeRequest(videoId, userId);
  }
  addViewLater() {
    this.props.addWatchLaterRequest(videoId, userId);
  }
}

const s = StyleSheet.create({
  list: {
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap"
  }
});

const mapDispatchToProps = dispatch => ({
  videoDetail: (language_id, video_id, user_id, isRefreshOnly) => dispatch(videoDetail(language_id, video_id, user_id, isRefreshOnly)),
  addLikeRequest: (video_id, user_id) => dispatch(addLikeRequest(video_id, user_id)),
  addWatchLaterRequest: (video_id, user_id) => dispatch(addWatchLaterRequest(video_id, user_id))
});
const mapStateToProps = state => ({
  isLoading: state.serviceReducer.isLoading,
  isActionLoading: state.serviceReducer.isActionLoading,
  error: state.serviceReducer.error,
  videoDetailData: state.serviceReducer.videoDetailData,
  addLike: state.serviceReducer.data,
  addWatchLater: state.serviceReducer.data,
  user: state.serviceReducer.user,
  language_id: state.serviceReducer.languageId,
  refreshVideoDetail: state.serviceReducer.refreshVideoDetail
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailPage);
