import React from "react";
import { BackHandler } from "react-native";
import YouTube from "react-native-youtube";
import { connect } from "react-redux";
export default class FullScreenVideo extends React.Component {
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }
  handleBackPress = () => {
    this.props.navigation.goBack(); // works best when the goBack is async
    return true;
  };
  render() {
    const { params } = this.props.navigation.state;
    var videoID = params.videoId;

    return (
      <YouTube
        videoId={videoID} // The YouTube video ID
        play={true} // control playback of video with true/false
        fullscreen={true} // control whether the video should play in fullscreen or inline
        loop={true}
        // control whether the video should loop when ended
        apiKey="AIzaSyDobd_4OKUfDaazchfZXyu_H93U8Y_MEEc"
        showInfo={true}
        style={{ alignSelf: "stretch", height: "100%" }}
        // onReady={e => this.setState({ isReady: true })}
        // onChangeState={e => this.setState({ status: e.state })}
        // onChangeQuality={e => this.setState({ quality: e.quality })}
        // onError={e => this.setState({ error: e.error })}
      />
    );
  }
}
