import React, { Component } from "react";
import {
  View,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Alert,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  Share,
  BackHandler,
  ImageBackground
} from "react-native";

import {
  Container,
  Left,
  Right,
  Icon,
  Button,
  Spinner,
  Content
} from "native-base";
import VideoCardItem from "../../../Components/VideoCardItem";
import AppHeader from "../../../Components/AppHeader";
import YouTube from "react-native-youtube";
import { Colors } from "../../../Themes/";
import styles from "../../../Themes/styles";
import string from "../../../Themes/String";
import { connect } from "react-redux";
import { videoDetail } from "../../../Actions/ServiceCallAction";
import { addLikeRequest } from "../../../Actions/ServiceCallAction";
import { addWatchLaterRequest } from "../../../Actions/ServiceCallAction";
import ArtistView from "../../../Components/ArtistView";
var languageId, videoId, userId;
//import Orientation from 'react-native-orientation'
class DetailPage extends Component {
  state = {
    favIcon: 0,
    watchIcon: 0,
    videoItem: null,
    landscape: false,
    showVideo: false,
    isReady: false,
    status: "",
    quality: 0,
    error: ""
  };

  componentWillMount() {
    const { params } = this.props.navigation.state;
    this.setState({ videoItem: params.videoItem });

    userId = this.props.user.user_id;
    languageId = this.props.language_id;
    videoId = params.videoItem.id;
    //console.warn("userId: " + userId + "languageId: " + languageId + "videoId: " + videoId);
    this.props.videoDetail(params.videoItem.id);
  }
  componentDidMount() {
    //  Orientation.addOrientationListener(this._orientationDidChange)
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }

  componentWillUnmount() {
    //  Orientation.removeOrientationListener(this._orientationDidChange)
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }
  _orientationDidChange(orientation) {
    // console.warn(orientation)
  }
  handleBackPress = () => {
    this.props.navigation.goBack(); // works best when the goBack is async
    return true;
  };
  render() {
    // console.warn(this.props.refreshVideoDetail)
    var details = this.props.videoDetailData.video_details;
    const { isRefreshing } = this.state;
    var left = (
      <Left style={{ flex: 1 }}>
        <Button onPress={() => this.handleBackPress()} transparent>
          <Icon name="ios-arrow-back" />
        </Button>
      </Left>
    );
    var right = (
      <Right style={{ flex: 1 }}>
        <Button
          onPress={() => this.props.navigation.navigate("Search")}
          transparent
        >
          <Icon name="ios-search" />
        </Button>
      </Right>
    );
    {
      this.props.refreshVideoDetail && this.props.videoDetail(videoId);
    }
    return (
      <Container style={{ flex: 1 }}>
        <AppHeader left={left} right={right} title="Startube" />
        {details && (
          <ScrollView>
            {this.renderYoutubePlayer(details)}
            {this.renderArtistInfo(this.props.videoDetailData.artists)}
            {this.renderMoreVideo(this.props.videoDetailData.more_videos)}
          </ScrollView>
        )}
        {(this.props.isLoading || details == null) && (
          <ActivityIndicator size="large" style={styles.activityIndicator} color={Colors.statusBarColor} />
        )}
      </Container>
    );
  }
  renderNameWithActionButtons(details) {
    return (
      <View
        style={{
          paddingLeft: 5,
          paddingRight: 5,
          flex: 1,
          flexDirection: "row",
          justifyContent: "space-between",
          alignContent: "center",
          alignItems: "center"
        }}
      >
        <View style={{ flex: 0.7, flexDirection: "row" }}>
          <Text style={styles.nameTitle}>{details.title}</Text>
        </View>
        {/* //ahi thi start karvu */}
        <View
          style={{
            flex: 0.3,
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          {this.props.isActionLoading && (
            <ActivityIndicator size="small" color={Colors.primaryColor} />
          )}
          <Button onPress={() => this.addLike()} transparent>
            {this.props.videoDetailData.user_liked ? (
              <Icon name="ios-heart" style={styles.smallIconColored} />
            ) : (
              <Icon name="ios-heart" style={styles.smallIcon} />
            )}
          </Button>
          <Button onPress={() => this.addViewLater()} transparent>
            {this.props.videoDetailData.user_view_later ? (
              <Icon name="ios-time" style={styles.smallIconColored} />
            ) : (
              <Icon name="ios-time" style={styles.smallIcon} />
            )}
          </Button>
          <Button onPress={() => this.share()} transparent>
            <Icon name="md-share" style={styles.smallIcon} />
          </Button>
        </View>
      </View>
    );
  }
  share() {
    var details = this.props.videoDetailData;
    Share.share(
      {
        message: details.video_details.title,
        url: details.video_details.url,
        title: string.AppName
      },
      {
        // Android only:
        dialogTitle: "Share Video",
        // iOS only:
        excludedActivityTypes: ["com.apple.UIKit.activity.PostToTwitter"]
      }
    );
  }
  renderYoutubePlayer(vDetails) {
    console.warn(vDetails.video_id);
    return (
      <View style={{ flexDirection: "column" }}>
        <ImageBackground
          style={styles.videoCatalougeImage}
          source={{ uri: vDetails.thumbnail }}
        >
          <Button
            transparent
            style={styles.playIconContainer}
            onPress={() =>
              this.props.navigation.navigate("FullScreenVideo", {
                videoId: vDetails.video_id
              })
            }
          >
            <Icon size={50} name="play-circle" color="white" />
          </Button>
        </ImageBackground>

        {/* <YouTube
          videoId={vDetails.video_id} // The YouTube video ID
          play={true} // control playback of video with true/false
          hidden={false} // control visiblity of the entire view
          playsInline={true} // control whether the video should play inline
          apiKey="AIzaSyDobd_4OKUfDaazchfZXyu_H93U8Y_MEEc"
          // onReady={e => {
          //   this.setState({ isReady: true });
          // }}
          // onChangeState={e => {
          //   this.setState({ status: e.state });
          // }}
          // onChangeQuality={e => {
          //   this.setState({ quality: e.quality });
          // }}
          // onError={e => {
          //   this.setState({ error: e.error });
          // }}
          style={{
            alignSelf: "stretch",
            height: 300,
            backgroundColor: "black",
            marginVertical: 10
          }}
        /> */}

        {this.renderNameWithActionButtons(vDetails)}
      </View>
    );
  }
  artistDetailPage() {}
  renderArtistInfo(artists) {
    return (
      <View>
        {artists && (
          <View style={styles.detailPageBg}>
            <View style={styles.connectionHeaderBg}>
              <Text style={styles.connectionPhotosTxt}>Information</Text>
              <TouchableOpacity
                style={styles.connectionPhotoCountBg}
                onPress={() => alert("Artists")}
              />
            </View>
            <View style={styles.connectionProfileBg}>
              <FlatList
                horizontal
                data={artists}
                renderItem={({ item: rowData }) => (
                  <ArtistView
                    data={rowData}
                    navigation={this.props.navigation}
                  />
                )}
                keyExtractor={(item, index) => index}
              />
            </View>
          </View>
        )}
      </View>
    );
  }
  renderMoreVideo(morevideos) {
    return (
      <View>
        {morevideos.length > 0 && (
          <View style={styles.connectionBg}>
            <View style={styles.connectionHeaderBg}>
              <Text style={styles.connectionPhotosTxt}>Like Videos</Text>
              <TouchableOpacity
                style={styles.connectionPhotoCountBg}
                // onPress={() => alert("Artists")}
              />
            </View>
            <View style={styles.connectionProfileBg}>
              <FlatList
                horizontal
                data={morevideos}
                renderItem={({ item: rowData }) => {
                  return (
                    <VideoCardItem
                      data={rowData}
                      navigation={this.props.navigation}
                    />
                  );
                }}
                keyExtractor={(item, index) => index}
              />
            </View>
          </View>
        )}
      </View>
    );
  }
  addLike(video_id, user_id) {
    this.props.addLikeRequest(video_id, user_id);
  }
  addViewLater(video_id, user_id) {
    this.props.addWatchLaterRequest(video_id, user_id);
  }
}

const s = StyleSheet.create({
  list: {
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap"
  }
});

const mapDispatchToProps = dispatch => ({
  videoDetail: video_Id => dispatch(videoDetail(languageId, video_Id, userId)),
  addLikeRequest: () => dispatch(addLikeRequest(videoId, userId)),
  addWatchLaterRequest: () => dispatch(addWatchLaterRequest(videoId, userId))
});
const mapStateToProps = state => ({
  isLoading: state.serviceReducer.isLoading,
  isActionLoading: state.serviceReducer.isActionLoading,
  error: state.serviceReducer.error,
  videoDetailData: state.serviceReducer.videoDetailData,
  addLike: state.serviceReducer.data,
  addWatchLater: state.serviceReducer.data,
  user: state.serviceReducer.user,
  language_id: state.serviceReducer.languageId,
  refreshVideoDetail: state.serviceReducer.refreshVideoDetail
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailPage);
