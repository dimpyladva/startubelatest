import React, { Component } from "react";
import { connect } from "react-redux";
import { myFavVideosAPIRequest } from "../../../Actions/ServiceCallAction";
import {
  View,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Text,
  BackHandler
} from "react-native";
import { Container, Left, Right, Icon, Button, Content } from "native-base";
import VideoCardItem from "../../../Components/VideoCardItem";
import { ListItem, SearchBar } from "react-native-elements"; // 0.19.1
import AppHeader from "../../../Components/AppHeader";
import { Colors } from "../../../Themes/";
import styles from "../../../Themes/styles";
var userId;

class Favorites extends Component {
  state = {
    seed: 1,
    page: 1,
    last_page: 0,
    list: {},
    isLoading: false,
    isRefreshing: false,
    showTheSearchbar: false,
    loadMore: true,
    input: ""
  };

  loadData = () => {
    const { list, seed, page, last_page, isRefreshing } = this.state;
    this.setState({
      isLoading: true,
      loadMore: true
    });

    if (last_page >= page || last_page == 0) {
      // fetch(
      //   //'http://share.netispy.com/star_tube/api/v1/categories/videos?type=new&category_id=6&limit=5&page=${last_page}
      //   `http://share.netispy.com/star_tube/api/v1/categories/videos?type=new&category_id=6&limit=5&page=${page}`
      // )
      //   .then(res => res.json())
      this.props
        .myFavVideosAPIRequest(this.props.languageId,userId, this.state.page)
        .then(() => {
          let res = this.props.favorites;
          if (res.status == "success") {
            this.setState({
              list:
                last_page == 0 || isRefreshing
                  ? res.likelist_videos
                  : [...this.state.list, ...res.likelist_videos],
              page: res.page,
              last_page: res.last_page,
              isRefreshing: false,
              isLoading: false
            });
          } else {
            this.setState({ isLoading: false });
          }
        })
        .catch(err => {
          console.warn(err);
        });
    }
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        last_page: 0,
        isRefreshing: true
      },
      () => {
        this.loadData();
      }
    );
  };

  handleLoadMore = () => {
    this.loadData();
  };
  componentDidUpdate(prevProps, prevState) {
    // if(prevProps.isFavouriteUpdated != this.props.isFavouriteUpdated && this.props.isFavouriteUpdated){
    //   this.loadData();
    // }
  }
  componentWillMount() {
    const { params } = this.props.navigation.state;
    userId = this.props.user.user_id;
  }
  componentDidMount() {
    console.log("componentDidMount")
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    this.loadData();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }
  handleBackPress = () => {
    this.props.navigation.goBack(); // works best when the goBack is async
    return true;
  };

  renderHeader = () => {
    return (
      <View>
        {this.state.showTheSearchbar && (
          <SearchBar
            onChangeText={text => this.searchData(text)}
            placeholder="Type Here..."
            lightTheme
            round
          />
        )}
      </View>
    );
  };
  // <Header searchBar rounded>
  //   <Item>
  //     <Icon name="ios-search" />
  //     <Input placeholder="Search" />
  //     <Icon name="ios-people" />
  //   </Item>
  //   <Button transparent>
  //     <Text>Search</Text>
  //   </Button>
  // </Header>
  //
  renderFooter = () => {
    // console.warn(this.state.page + " last:" + this.state.last_page);

    if (!this.state.isLoading || this.state.page > this.state.last_page)
      return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator color={Colors.statusBarColor} animating size="large" />
      </View>
    );
  };

  render() {
    var that = this;
    const { list, isRefreshing } = this.state;
    // var left = (
    //   <Left style={{ flex: 1 }}>
    //     <Button onPress={() => this.handleBackPress()} transparent>
    //       <Icon name="ios-arrow-back" />
    //     </Button>
    //   </Left>
    // );
    var left = (
      <Left style={{ flex: 1 }}>
        <Button
          transparent
          onPress={() => this.props.navigation.openDrawer()}
        >
          <Icon name="md-menu" />
        </Button>
      </Left>
    );
    var right = (
      <Right style={{ flex: 1 }}>
        <Button
          onPress={() => this.props.navigation.navigate("Home")}
          transparent
        >
          <Icon name="ios-home" />
        </Button>
        <Button
          onPress={() => this.props.navigation.navigate("Search")}
          transparent
        >
          <Icon name="ios-search" />
        </Button>

      </Right>
    );
    return (
      <Container style={styles.container}>
        <AppHeader left={left} right={right} title="My Favorites" />

        {this.props.isLoading && (
          <ActivityIndicator size="large" style={styles.activityIndicator} color={Colors.statusBarColor} />
        )}
        {!this.props.isLoading &&
          <View style={s.scene}>
            {list && list.length > 0 && (
              <FlatList
                horizontal={false}
                numColumns={2}
                contentContainerStyle={styles.videoContainerStyle}
                data={list}
                renderItem={({ item: rowData }) => (
                  <VideoCardItem data={rowData} isSmall={true} {...this.props} />
                )}
                ListHeaderComponent={this.renderHeader}
                ListFooterComponent={this.renderFooter}
                keyExtractor={i => i.email}
                refreshing={isRefreshing}
                onRefresh={this.handleRefresh}
                onEndReached={
                  this.state.loadMore == true ? this.handleLoadMore : null
                }
                onEndThreshold={0}
              />
            )}
          </View>
        }

      </Container>
    );
  }
}

const s = StyleSheet.create({
  scene: {
    flex: 1
  },
  list: {
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap"
  }
});

const mapDispatchToProps = dispatch => ({
  myFavVideosAPIRequest: (languageId,userId, page) =>
    dispatch(myFavVideosAPIRequest(languageId,userId, page))
});
const mapStateToProps = state => ({
  isLoading: state.serviceReducer.isLoading,
  error: state.serviceReducer.error,
  favorites: state.serviceReducer.favorites,
  isFavouriteUpdated: state.serviceReducer.isFavouriteUpdated,
  languageId: state.serviceReducer.languageId,
  user: state.serviceReducer.user
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Favorites);
