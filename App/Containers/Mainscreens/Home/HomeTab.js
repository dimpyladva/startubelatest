import React, { Component } from "react";
import {
  Text,
  TouchableOpacity,
  View,
  FlatList,
  ScrollView
} from "react-native";
import { CachedImage } from 'react-native-cached-image';
import { Container, Spinner } from "native-base";
import { Colors } from "../../../Themes/";
import Swiper from "react-native-swiper";
import styles from "../../../Themes/styles";
import VideoCardItem from "../../../Components/VideoCardItem";
import ArtistSimpleView from "../../../Components/ArtistSimpleView";
export default class HomeTab extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    //var homeData = this.props.homeData;
  }
  handlePressViewAll(screenName, type, id) {
    this.props.navigation.navigate(screenName, { type: type, categoryId: id });
  }
  handlePressItem(screenName, videoItem) {
    this.props.navigation.navigate(screenName, { videoItem: videoItem });
  }
  render() {
    const { navigate } = this.props.navigation;
    var that = this;
    return (
      <Container style={styles.main}>
        <ScrollView>{this.renderTabContent()}</ScrollView>
      </Container>
    );
  }
  renderTabContent() {
    
    if (this.props.isTabLoading) {
      return <Spinner size="small" color={Colors.statusBarColor} />;
    } else {
      return (
        <View>
          {this.props.homeData.sliders &&
            this.props.homeData.sliders.length > 0 &&
            this.renderSlider()}
       
          <View style={{ marginTop: 10, flexDirection: "column" }}>
            {this.props.homeData.category_videos &&
              this.props.homeData.category_videos.length > 0 &&
              this.props.homeData.category_videos.map((data, i) => {
                return this.renderVideos(data, i);
              })}
            {this.props.homeData.artists &&
              this.props.homeData.artists.length > 0 &&
              this.props.homeData.artists.map((data, i) => {
                return this.renderArtists(data, i);
              })}
          </View>
        </View>
      );
    }
  }
  artistViewAll(data) {
    this.props.navigation.navigate("ArtistList", { artistData: data });
  }
  renderSlider() {
    return (
      <View>
        <Swiper
          style={styles.slidesec}
          removeClippedSubviews={false}
          showsButtons={false}
          autoplay={true}
          autoplayTimeout={3}
          activeDot={<View style={styles.activeDot} />}
          dot={<View style={styles.dot} />}
        >
          {this.props.homeData.sliders.map((item, index) => {
            return (
              item.thumbnail &&
              <View style={styles.slide} key={index}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("VideoDetail", {
                      videoItem: item
                    })
                  }
                >
                  <CachedImage
                    source={{ uri: item.thumbnail }}
                    style={styles.sliderImage}
                    activityIndicatorProps = {styles.imageActivityIndicator}
                  />
                </TouchableOpacity>
              </View>
            );
          })}
        </Swiper>
      </View>
    );
  }
  renderVideos(videoData, index) {
    return (
      videoData.videos &&
      videoData.videos.length > 0 && (
        <View style={styles.connectionBg} key={index}>
          <View style={styles.connectionHeaderBg}>
            <View style={styles.connectionHeaderBgRow}>
              <View style={styles.verticalLine} />
              <Text style={styles.connectionPhotosTxt}>{videoData.name}</Text>
            </View>
            <TouchableOpacity
              onPress={() =>
                this.handlePressViewAll("ViewAll", "latest", videoData.id)
              }
              style={styles.connectionPhotoCountBg}
            >
              <Text style={styles.connectionPhotosCountTxt}>
                {this.props.languageConstants.LBL_VIEW_ALL}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.connectionProfileBg}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={videoData.videos}
              renderItem={({ item: rowData, i }) => {
                return (
                  <VideoCardItem
                    data={rowData}
                    navigation={this.props.navigation}
                  />
                );
              }}
              keyExtractor={item => item.id}
            />
          </View>
        </View>
      )
    );
  }

  renderArtists(artistData, index) {
    return (
      artistData.artists &&
      artistData.artists.length > 0 && (
        <View style={styles.connectionBg} key={index}>
          <View style={styles.connectionHeaderBg}>
            <View style={{ flexDirection: "row" }}>
              <View style={styles.verticalLine} />
              <Text style={styles.connectionPhotosTxt}>{artistData.name}</Text>
            </View>

            <TouchableOpacity
              style={styles.connectionPhotoCountBg}
              onPress={() => this.artistViewAll(artistData)}
            >
              <Text style={styles.connectionPhotosCountTxt}>
                {this.props.languageConstants.LBL_VIEW_ALL}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.connectionProfileBg}>
            <FlatList
              horizontal
              data={artistData.artists}
              renderItem={({ item: rowData }) => {
                return (
                  <ArtistSimpleView
                    data={rowData}
                    navigation={this.props.navigation}
                  />
                );
              }}
              keyExtractor={item => item.id}
            />
          </View>
        </View>
      )
    );
  }
}
