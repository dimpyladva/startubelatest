import React, { Component } from "react";
import {
  Text,
  TouchableOpacity,
  View,
  FlatList,
  ScrollView
} from "react-native";
import { CachedImage } from 'react-native-cached-image';
import { Container, Spinner } from "native-base";
import { Colors } from "../../../Themes/";
import Swiper from "react-native-swiper";
import styles from "../../../Themes/styles";
import VideoCardItem from "../../../Components/VideoCardItem";
import ArtistSimpleView from "../../../Components/ArtistSimpleView";

export default class Tablist extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    // console.log("Tab")
    // console.log(this.props.isTabLoading)
    // console.log(this.props.category)
    // console.log("catId: " + this.props.category.id);
    var categoryId = this.props.category.id;

    if (
      !this.props.category.tabData ||
      (this.props.category.tabData && this.props.category.tabData == null)
    ) {
      this.props.tabViewData(categoryId, this.props.languageId);
    }
  }

  handlePressViewAll(screenName, type) {
    this.props.navigation.navigate(screenName, {
      type: type,
      categoryId: this.props.category.id
    });
  }
  handlePressItem(screenName, videoItem) {
    this.props.navigation.navigate(screenName, { videoItem: videoItem });
  }

  render() {
    const { navigate } = this.props.navigation;
    var that = this;
    return (
      <Container style={styles.main}>
        <ScrollView>{this.renderTabContent()}</ScrollView>
      </Container>
    );
  }
  renderTabContent() {
    console.log(this.props.category)
    if (this.props.isTabLoading || !this.props.category.tabData) {
      return <Spinner size="small" color={Colors.statusBarColor} />;
    } else {
      console.log(this.props.category.tabData.sliders)
      console.log(this.props.category.tabData.new_videos)
      console.log(this.props.category.tabData.trending_videos)
      console.log(this.props.category.tabData.popular_videos)
      console.log(this.props.category.tabData.artist_widget)



      //console.warn("Slider Length: " + this.props.category.tabData.sliders.length)
      if (
        this.props.category.tabData.sliders.length == 0 &&
        this.props.category.tabData.new_videos.length == 0 &&
        this.props.category.tabData.trending_videos.length == 0 &&
        this.props.category.tabData.popular_videos.length == 0 &&
        Object.keys(this.props.category.tabData.artist_widget).length==0) {
          return (
            <Text style={styles.containerMessage}>No Records</Text>
          )
      }
      return (
        <View>
          {this.props.category.tabData.sliders.length > 0 &&
            this.renderSlider()}
          {this.props.category.tabData.new_videos.length > 0 &&
            this.renderNewVideos()}
          {this.props.category.tabData.trending_videos.length > 0 &&
            this.renderTrendingVideos()}
          {this.props.category.tabData.popular_videos.length > 0 &&
            this.renderPopularVideos()}
          {this.props.category.tabData.artist_widget &&
            this.renderArtists(this.props.category.tabData.artist_widget)}
        </View>
      );
    }
  }
  renderSlider() {
    return (

      this.props.category.tabData.length > 0 &&

      <View>
        <Swiper
          style={styles.slidesec}
          removeClippedSubviews={false}
          showsButtons={false}
          autoplay={true}
          autoplayTimeout={3}
          activeDot={<View style={styles.activeDot} />}
          dot={<View style={styles.dot} />}
        >
          {this.props.category.tabData.sliders.map((item, index) => {
            return (
              item.thumbnail &&
              <View style={styles.slide} key={index}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("VideoDetail", {
                      videoItem: item
                    })
                  }
                >
                  <CachedImage
                    source={{ uri: item.thumbnail }}
                    style={styles.sliderImage}
                    activityIndicatorProps={styles.imageActivityIndicator}
                  />
                </TouchableOpacity>
              </View>
            );
          })}
        </Swiper>
      </View>

    );
  }
  //   <TouchableOpacity
  //   onPress={() =>
  //     this.props.navigation.navigate("VideoDetail", {
  //       videoItem: item
  //     })
  //   }
  // >
  //   <View style={styles.slide} key={index}>
  //     <Image
  //       source={{ uri: item.thumbnail }}
  //       style={styles.sliderImage}
  //     />
  //   </View>
  // </TouchableOpacity>
  renderNewVideos() {
    return (
      <View style={styles.connectionBg}>
        <View style={styles.connectionHeaderBg}>
          <View style={{ flexDirection: "row" }}>
            <View style={styles.verticalLine} />
            <Text style={styles.connectionPhotosTxt}>
              {this.props.languageConstants.LBL_LATEST}
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => this.handlePressViewAll("ViewAll", "latest")}
            style={styles.connectionPhotoCountBg}
          >
            <Text style={styles.connectionPhotosCountTxt}>
              {this.props.languageConstants.LBL_VIEW_ALL}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.connectionProfileBg}>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={this.props.category.tabData.new_videos}
            renderItem={({ item: rowData }) => {
              return (
                <VideoCardItem
                  data={rowData}
                  navigation={this.props.navigation}
                />
              );
            }}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    );
  }
  renderTrendingVideos() {
    return (
      <View style={styles.connectionBg}>
        <View style={styles.connectionHeaderBg}>
          <View style={{ flexDirection: "row" }}>
            <View style={styles.verticalLine} />
            <Text style={styles.connectionPhotosTxt}>
              {this.props.languageConstants.LBL_TRENDING}
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => this.handlePressViewAll("ViewAll", "latest")}
            style={styles.connectionPhotoCountBg}
          >
            <Text style={styles.connectionPhotosCountTxt}>
              {this.props.languageConstants.LBL_VIEW_ALL}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.connectionProfileBg}>
          <FlatList
            horizontal
            data={this.props.category.tabData.trending_videos}
            renderItem={({ item: rowData }) => {
              return (
                <VideoCardItem
                  data={rowData}
                  navigation={this.props.navigation}
                />
              );
            }}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    );
  }
  renderPopularVideos() {
    return (
      <View style={styles.connectionBg}>
        <View style={styles.connectionHeaderBg}>
          <View style={{ flexDirection: "row" }}>
            <View style={styles.verticalLine} />
            <Text style={styles.connectionPhotosTxt}>
              {this.props.languageConstants.LBL_POPULAR}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => this.handlePressViewAll("ViewAll", "latest")}
            style={styles.connectionPhotoCountBg}
          >
            <Text style={styles.connectionPhotosCountTxt}>
              {this.props.languageConstants.LBL_VIEW_ALL}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.connectionProfileBg}>
          <FlatList
            horizontal
            data={this.props.category.tabData.popular_videos}
            renderItem={({ item: rowData }) => {
              return (
                <VideoCardItem
                  data={rowData}
                  navigation={this.props.navigation}
                />
              );
            }}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    );
  }
  renderArtists(artist_widget) {
    return (
      artist_widget &&
      artist_widget.artists &&
      artist_widget.artists.length > 0 && (
        <View style={styles.connectionBg}>
          <View style={styles.connectionHeaderBg}>
            <View style={{ flexDirection: "row" }}>
              <View style={styles.verticalLine} />
              <Text style={styles.connectionPhotosTxt}>
                {artist_widget.name}
              </Text>
            </View>

            <TouchableOpacity
              style={styles.connectionPhotoCountBg}
              onPress={() => this.artistViewAll(artist_widget)}
            >
              <Text style={styles.connectionPhotosCountTxt}>
                {this.props.languageConstants.LBL_VIEW_ALL}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.connectionProfileBg}>
            <FlatList
              horizontal
              data={artist_widget.artists}
              renderItem={({ item: rowData }) => {
                return (
                  <ArtistSimpleView
                    data={rowData}
                    navigation={this.props.navigation}
                  />
                );
              }}
              keyExtractor={item => item.id}
            />
          </View>
        </View>
      )
    );
  }
  artistViewAll(data) {
    this.props.navigation.navigate("ArtistList", { artistData: data });
  }
}
