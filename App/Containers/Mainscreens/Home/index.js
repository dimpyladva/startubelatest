import React, { Component } from "react";
import { Platform } from "react-native";
import { connect } from "react-redux";
import { Container, Right, Left, Icon, Button, Spinner, Content } from "native-base";
import { getCategories } from "../../../Actions/ServiceCallAction";
import AppHeader from "../../../Components/AppHeader";
import ScrollableTabView, {
  ScrollableTabBar
} from "../../../Components/react-native-scrollable-tab-view";
// Screen Styles
import styles from "../../../Themes/styles";
import { tabViewData } from "../../../Actions/ServiceCallAction";
import { homeDataRequest } from "../../../Actions/ServiceCallAction";
import { Colors } from "../../../Themes/";
import CategoryTab from "./CategoryTab";
import HomeTab from "./HomeTab";
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0
    };
  }
  componentWillMount() {
    this.props.getCategories(this.props.languageId);
    this.props.homeDataRequest(this.props.languageId);
  }
  handleChangeTab(i, ref, from) {
    //console.warn(i)
  }
  render() {
    var that = this;
    var left = (
      <Left style={{ flex: 0.5 }}>
        <Button
          transparent
          onPress={() => this.props.navigation.openDrawer()}
        >
          <Icon name="md-menu" />
        </Button>
      </Left>
    );
    var right = (
      <Right style={{ flex: 0.5 }}>
        <Button
          transparent
          onPress={() => this.props.navigation.navigate("Search")}
        >
          <Icon name="ios-search" />
        </Button>
      </Right>
    );
    return (
      <Container style={styles.container}>
        <AppHeader left={left} right={right} title="StarTube" />
        {this.props.isLoading && (
          <Spinner size="small" color={Colors.statusBarColor} />
        )}

        {!this.props.isLoading && (
          <ScrollableTabView
            tabBarUnderlineStyle={styles.tabUnderLine}
            tabBarBackgroundColor={Colors.tabBarBackgroundColor}
            tabBarActiveTextColor={Colors.tabBarActiveTextColor}
            tabBarInactiveTextColor={Colors.tabBarInactiveTextColor}
            tabBarTextStyle={styles.tabText}
            page={this.state.index}
            initialPage={this.state.index}
            onChangeTab={({ i, ref }) => this.setState({ index: i })} //i: the index of the tab-selected, ref: the ref of the tab-selected
            renderTabBar={(...props) => (
              <ScrollableTabBar
                style={{
                  paddingTop: 5,
                  height: 60,
                  shadowColor: "#000",
                  shadowOffset: { width: 0, height: 0 },
                  shadowOpacity: 0,
                  shadowRadius: 0,
                  elevation: 0,
                  borderWidth: 0,
                  borderColor: "transparent"
                }}
              />
            )}
          >

              {this.props.homeData && (
                <HomeTab
                  isTabLoading={this.props.isTabLoading}
                  navigation={this.props.navigation}
                  languageConstants={this.props.languageConstants}
                  tabLabel="Home"
                  homeData={this.props.homeData}
                />
              )}
              {this.props.categories != null &&
                this.props.categories.length > 0 &&
                this.props.categories.map((category, index) => (
                  <CategoryTab
                    key={index}
                    isTabLoading={this.props.isTabLoading}
                    navigation={this.props.navigation}
                    tabViewData={this.props.tabViewData}
                    languageId={this.props.languageId}
                    category={category}
                    languageConstants={this.props.languageConstants}
                    tabLabel={category.name}
                    homeData={this.props.homeData}
                  />
                ))}

          </ScrollableTabView>
        )}
      </Container>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  getCategories: languageId => dispatch(getCategories(languageId)),
  tabViewData: (categoryId,languageId) => dispatch(tabViewData(categoryId,languageId)),
  homeDataRequest: (languageId) => dispatch(homeDataRequest(languageId))
});
const mapStateToProps = state => ({
  isLoading: state.serviceReducer.isLoading,
  isTabLoading: state.serviceReducer.isTabLoading,
  error: state.serviceReducer.error,
  categories: state.serviceReducer.categories,
  languageConstants: state.serviceReducer.languageConstants,
  languageId: state.serviceReducer.languageId,
  homeData: state.serviceReducer.homeData
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);

{
  /* <Tablist navigation={this.props.navigation} tabLabel="Home" />
<Tablist navigation={this.props.navigation} tabLabel="Drama" />
<Tablist navigation={this.props.navigation} tabLabel="Comedy" />
<Tablist navigation={this.props.navigation} tabLabel="Film" /> */
}
// tabcode(){
//   <ScrollableTabView
//     initialPage={0}
//     tabBarUnderlineStyle={{ backgroundColor: "#0691ce" }}
//     tabBarBackgroundColor={"#f7f7f7"}
//     tabBarActiveTextColor={"#0691ce"}
//     tabBarInactiveTextColor={"#d9d9d9"}
//     tabBarTextStyle={styles.tabText}
//     renderTabBar={() => (
//   <ScrollableTabBar
//     style={{
//       shadowColor: "#000",
//       shadowOffset: { width: 0, height: 0 },
//       shadowOpacity: 0,
//       shadowRadius: 0,
//       elevation: 0,
//       borderWidth: 0,
//       borderColor: "transparent"
//     }}
//   />
// )}
// >
// <View tabLabel="Photos">
//   <View
//     style={[styles.container, { backgroundColor: "transparent" }]}
//   >
//     <Tablist />
//   </View>
// </View>
//
// <View tabLabel="Videos">
//   <ListView
//     contentContainerStyle={styles.listContent}
//     dataSource={this.state.dataSource}
//     renderRow={this._renderRow.bind(this)}
//     renderSeparator={this._renderSeparator}
//     enableEmptySections
//     pageSize={4}
//   />
// </View>
//
// <View tabLabel="Events">
//   <View
//     style={[styles.container, { backgroundColor: "transparent" }]}
//   >
//     <Tablist />
//   </View>
// </View>
//
// <View tabLabel="Books">
//   <View
//     style={[styles.container, { backgroundColor: "transparent" }]}
//   >
//     <Tablist />
//   </View>
// </View>
// </ScrollableTabView>
// }
