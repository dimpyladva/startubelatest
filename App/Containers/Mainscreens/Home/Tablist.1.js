import React, { Component } from "react";
import {
  Text,
  Image,

  Platform,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  View,
  ListView,
  FlatList,
  ScrollView
} from "react-native";
import {
  Container,
  Button,
  Right,
  Left,
  Content,
  Body,
  Header,
  Icon,
  Title,
  RegularText
} from "native-base";
import ReadMore from "react-native-read-more-text";
import { connect } from "react-redux";
import { Images, Colors, Fonts, Metrics } from "../../../Themes/";
import Swiper from "react-native-swiper";
import styles from "../../../Themes/styles";
import VideoCardItem from "../../../Components/VideoCardItem";
import Ionicons from "react-native-vector-icons/Ionicons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
/**
 *  Profile Screen
 */
const bgImage =
  "https://antiqueruby.aliansoftware.net//Images/profile/backg_profile_11.png";
const profileImg = "https://i.ytimg.com/vi/ryIVK7_XUP4/maxresdefault.jpg";
const profileImgOne =
  "https://antiqueruby.aliansoftware.net//Images/profile/ic_user1.png";
const profileImgTwo =
  "https://antiqueruby.aliansoftware.net//Images/profile/ic_user2.png";
const profileImgThree =
  "https://antiqueruby.aliansoftware.net//Images/profile/ic_user3.png";
const profileImgFour =
  "https://antiqueruby.aliansoftware.net//Images/profile/ic_user4.png";
const profileImgFive =
  "https://antiqueruby.aliansoftware.net//Images/profile/ic_user5.png";
const profileImageTwo = "https://i.ytimg.com/vi/k9972Zeqjy4/maxresdefault.jpg";
const profileImageThree =
  "https://i.ytimg.com/vi/uNyn9sCzoTU/maxresdefault.jpg";
const profileImageFour = "https://i.ytimg.com/vi/zTjy5dI3gp4/maxresdefault.jpg";
const profileImageFive = "https://i.ytimg.com/vi/QbdpI5hriCU/maxresdefault.jpg";
const profileImageSix = "https://i.ytimg.com/vi/ce_UsEIYAo8/maxresdefault.jpg";
const profileImageSeven =
  "https://i.ytimg.com/vi/vHdR0zWDK6Q/maxresdefault.jpg";
const profileImageEight =
  "https://i.ytimg.com/vi/v6C9pSRJTws/maxresdefault.jpg";
const profileImageNine =
  "https://gujuwood.com/wp-content/uploads/2017/01/sddefault-13.jpg";
let swiperImage = {
  uri: "https://i.ytimg.com/vi/QbdpI5hriCU/maxresdefault.jpg"
};
let swiperImage2 = {
  uri: "https://i.ytimg.com/vi/ryIVK7_XUP4/maxresdefault.jpg"
};
let swiperImage3 = {
  uri: "https://i.ytimg.com/vi/ce_UsEIYAo8/maxresdefault.jpg"
};
var data = [
  {
    id: 1,
    image: swiperImage
  },
  {
    id: 2,
    image: swiperImage2
  },
  {
    id: 3,
    image: swiperImage3
  }
];
var artistsData = [
  { image: profileImgOne },
  { image: profileImgTwo },
  { image: profileImgThree },
  { image: profileImgFour },
  { image: profileImgFive },
  { image: profileImgOne }
];
class Tablist extends Component {
  constructor(props) {
    super(props);

    const profileDetails = [
      {
        id: 1,
        profileImage: { uri: profileImg },
        name: "Johnie Cornwall",
        designation: "Senior Design Director"
      },
      {
        id: 2,
        profileImage: { uri: profileImageTwo },
        name: "Renaldo Rozman",
        designation: "Lead 3D Artist"
      },
      {
        id: 3,
        profileImage: { uri: profileImageThree },
        name: "Argelia Bee",
        designation: "Copywriter"
      },
      {
        id: 4,
        profileImage: { uri: profileImageFour },
        name: "Kimiko Hoyle",
        designation: "Marketing & Creative Services"
      },
      {
        id: 5,
        profileImage: { uri: profileImageFive },
        name: "Argelia Bee",
        designation: "Copywriter"
      },
      {
        id: 6,
        profileImage: { uri: profileImageSix },
        name: "Johnie Cornwall",
        designation: "Copywriter"
      },
      {
        id: 7,
        profileImage: { uri: profileImageSeven },
        name: "Tim Droney",
        designation: "Copywriter"
      }
    ];

    const rowHasChanged = (r1, r2) => r1 !== r2;
    const ds = new ListView.DataSource({ rowHasChanged });

    this.state = {
      isLoading: true,
      profileDetailsSource: ds.cloneWithRows(profileDetails),
      data: artistsData
    };
  }
  componentWillMount() {
    var that = this;
  }
  _renderProfileDetailRow(rowData) {
    return (
      <View style={{ flexDirection: "column" }}>
        <View style={styles.mainRow}>
          <Image source={rowData.profileImage} style={styles.profileImg} />
          <View style={styles.nameDesignationMainView}>
            <View style={styles.nameDesignationView}>
              <Text style={styles.nameTxt}>{rowData.name}</Text>
              <Text style={styles.designationTxt}>{rowData.designation}</Text>
            </View>
          </View>
        </View>

        <View style={styles.dividerHorizontal} />
      </View>
    );
  }
  _handlePress(screenName) {
    var tempVar = "";
    // if(screenName == 'Home') {
    //   tempVar = Images.ic_select
    // }
    //  else if(screenName == 'SelectNewChildToPlayScreen'){
    //   tempVar = Images.ic_selectnewchildActive
    // } else if(screenName == 'ViewScoreScreen'){
    //   tempVar = Images.ic_ViewScoreActive
    // } else if(screenName == 'SettingsScreen'){
    //   tempVar = Images.ic_settingActive
    // }

    this.props.navigation.navigate(screenName);
  }

  render() {
    const { navigate } = this.props.navigation;
    var that = this;
    // StatusBar.setBarStyle("light-content", true);
    // if (Platform.OS === "android") {
    //   StatusBar.setBackgroundColor("transparent", true);
    //   StatusBar.setTranslucent(true);
    // }

    return (
      <Container style={styles.main}>
        <ScrollView>

          <Swiper
            style={styles.slidesec}
            showsButtons={false}
            autoplay={true}
            autoplayTimeout={3}
            activeDot={<View style={styles.activeDot} />}
            dot={<View style={styles.dot} />}
          >
            {data.map((item, index) => {
              return (
                <View style={styles.slide} key={index}>
                  <Image source={item.image} style={styles.sliderImage} />
                </View>
              );
            })}
          </Swiper>


          <View style={styles.connectionBg}>
            <View style={styles.connectionHeaderBg}>
              <Text style={styles.connectionPhotosTxt}>Artists</Text>
              <TouchableOpacity
                style={styles.connectionPhotoCountBg}
                onPress={() => navigate('ViewAll')}
              >
                <Text style={styles.connectionPhotosCountTxt}>See all</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.connectionProfileBg}>
              <FlatList
                horizontal
                data={this.state.data}
                renderItem={({ item: rowData }) => {
                  return (
                    <View style={styles.connectionProfileSocialBg}>

                      <Image
                        source={{ uri: rowData.image }}
                        style={styles.connectionUserImg}
                      />
                    </View>
                  );
                }}
                keyExtractor={(item, index) => index}
              />
            </View>
          </View>
          <View style={styles.connectionBg}>
            <View style={styles.connectionHeaderBg}>
              <Text style={styles.connectionPhotosTxt}>Trending</Text>
              <TouchableOpacity
                onPress={() => that._handlePress("DetailPage")}
                style={styles.connectionPhotoCountBg}
              >
                <Text style={styles.connectionPhotosCountTxt}>See all</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.connectionProfileBg}>
              <FlatList
                horizontal
                data={this.state.data}
                renderItem={({ item: rowData }) => {
                  return (
                    <VideoCardItem
                      id={"0"}
                      title={"Video Name"}
                      image={rowData.image}
                      onPress={() => that._handlePress("DetailPage")}
                    />
                  );
                }}
                keyExtractor={(item, index) => index}
              />
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}
// <View style={styles.connectionBg}>
//   <View style={styles.connectionHeaderBg}>
//     <Text style={styles.connectionPhotosTxt}>Trending Videos</Text>
//     <TouchableOpacity
//       style={styles.connectionPhotoCountBg}
//       onPress={() => alert("See More Trending")}
//     >
//       <Text style={styles.connectionPhotosCountTxt}>See all</Text>
//     </TouchableOpacity>
//   </View>
//   <View style={styles.connectionProfileBg}>
//     <FlatList
//       horizontal
//       data={this.profileDetails}
//       renderItem={({ item: rowData }) => {
//         return (
//           <Product
//             id={"0"}
//             title={"Video Name"}
//             image={rowData.profileImage}
//           />
//         );
//       }}
//       keyExtractor={(item, index) => index}
//     />
//   </View>
// </View>
const mapStateToProps = state => {
  return {
    // ...redux state to props here
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Tablist);
