import React, { Component } from "react";
import { FlatList, View, Text, Alert } from "react-native";
import { CheckBox } from "react-native-elements";
import { Button, Left, Right, Icon, Container, Content } from 'native-base';
import { connect } from "react-redux";
import styles from "../../../Themes/styles"
import { getlanguageList, setLanguage, getLabels } from '../../../Actions/ServiceCallAction';
import AppHeader from '../../../Components/AppHeader';

class Language extends Component {
    constructor(props) {
        super(props);
        let isFirstTimeload = false;
        if (this.props.navigation.state.params && this.props.navigation.state.params.first) {
            isFirstTimeload = true;
        }
        this.state = {
            languages: [],
            isFirstTimeload: isFirstTimeload,
            language: this.props.languageId,
        }
    }
    componentWillMount() {
        this.props.getlanguageList().then(() => {
            this.setState({ languages: this.props.languageList });
        });
    }
    onPressDone() {
        let language = this.state.language;
        if (!language) {
            Alert.alert("Please select Language.")
            return;
        }
        this.props.setLanguage(language).then(() => {
            this.props.navigation.navigate('Home');
        });
    }
    render() {
        var left = (<Left style={{ flex: 0.5 }}><Button transparent onPress={() => this.props.navigation.openDrawer()}><Icon name='md-menu' /></Button></Left>);
        var right = (<Right style={{ flex: (!this.state.isFirstTimeload ? 0.5 : 1) }}><Button transparent onPress={() => this.onPressDone()}><Icon name='md-done-all' /></Button></Right>);
        return (
            <Container style={styles.container}>
                <AppHeader left={!this.state.isFirstTimeload ? left : null} right={right} title="Change Language" />

                {this.state.languages &&
                    <View style={styles.checkboxContainerStyle}>
                        <FlatList
                            data={this.state.languages}
                            selected={this.state.language}
                            keyExtractor={(item, index) => index}
                            renderItem={({ item, index }) => (
                                this._renderRow(item, index)
                            )} />
                    </View>
                }
            </Container>
        );
    }
    _renderRow(rowData, rowID) {
        return (
            <View>
                <CheckBox
                    iconLeft
                    checked={(this.state.language && rowData.id == this.state.language)}
                    title={rowData.alias}
                    onPress={() => this.checkedPress(rowData, rowID)}
                />
            </View>
        );
    }
    checkedPress(data, index) {
        //console.log("CheckPress")
        this.setState({ language: data.id })
    }
}
const mapDispatchToProps = (dispatch) => ({
    getlanguageList: () => dispatch(getlanguageList()),
    setLanguage: (languageId) => dispatch(setLanguage(languageId))
});
const mapStateToProps = (state) => ({
    isLoading: state.serviceReducer.isLoading,
    error: state.serviceReducer.error,
    languageList: state.serviceReducer.languageList,
    languageId: state.serviceReducer.languageId
});


export default connect(mapStateToProps, mapDispatchToProps)(Language);
