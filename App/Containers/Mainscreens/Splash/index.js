import React, { Component } from "react";
import { Platform, StatusBar, Image, ImageBackground } from "react-native";
import {
  Container,
  Text,
  Button,
  Header,
  Right,
  Left,
  Body,
  View,
  Spinner
} from "native-base";
import { NavigationActions, StackActions } from "react-navigation";
import { connect } from "react-redux";
import DeviceInfo from "react-native-device-info";
import FadeInView from "../../../Components/FadeInView";
import {
  login,
  getGeneralDetails,
  setLanguage
} from "../../../Actions/ServiceCallAction";
import styles from "../../../Themes/styles";
import { Metrics } from "../../../Themes";
const ic_logo = require("../../../Images/Logo.png");

class Splash extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    if (Object.keys(this.props.user).length === 0) {
      var device_id = DeviceInfo.getUniqueID();
      var device_type = Platform.OS == "ios" ? "i" : "a";
      //console.warn("no user")
      this.props.login(device_id, device_type).then(() => {
        //console.warn("yes")
        this.navigateToNextPage();
      });
    } else {
      console.warn("yes user")
      this.navigateToNextPage();
    }
  }
  navigateToAnotherStack(key, routeName, keyParams = null, routeParams = null) {
    const actionToDispatch = StackActions.reset({
      index: 0,
      key: null,
      actions: [
        NavigationActions.navigate({
          routeName: key, // Call home stack
          params: keyParams,
          action: NavigationActions.navigate({
            routeName: routeName,
            params: routeParams // Navigate to this screen
          })
        })
      ]
    });
    this.props.navigation.dispatch(actionToDispatch);
  }
  async navigateToNextPage() {
    await this.props.getGeneralDetails();
    if (this.props.languageId != null) {
      this.props.setLanguage(this.props.languageId).then(() => {
        this.navigateToAnotherStack("LaunchScreen", "Home");
      });
    } else {
      this.navigateToAnotherStack("LaunchScreen", "Language", null, {
        first: true
      });
    }
  }
  render() {
    var that = this;
    //StatusBar.setBarStyle("light-content", true);
    // if (Platform.OS === "android") {
    //   StatusBar.setBackgroundColor("transparent", true);
    //   StatusBar.setTranslucent(true);
    // }
    return (
      <Container style={styles.splashContainer}>
        <ImageBackground style={{ height: Metrics.HEIGHT, width: Metrics.WIDTH }}>
          <FadeInView style={{
            width: Metrics.WIDTH, height: Metrics.HEIGHT, 
            justifyContent: "center",
          }}>
            <Image source={ic_logo} style={styles.logo10} />
          </FadeInView>
        </ImageBackground>
      </Container >
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getGeneralDetails: () => dispatch(getGeneralDetails()),
  login: (device_id, device_type) => dispatch(login(device_id, device_type)),
  setLanguage: language_id => dispatch(setLanguage(language_id))
});
const mapStateToProps = state => ({
  isLoading: state.serviceReducer.isLoading,
  error: state.serviceReducer.error,
  user: state.serviceReducer.user,
  languageId: state.serviceReducer.languageId,
  _persist: state._persist
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Splash);
