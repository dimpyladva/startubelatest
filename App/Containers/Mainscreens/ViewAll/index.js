import React, { Component } from "react";
import { connect } from "react-redux";
import { viewAllVideosAPIRequest } from "../../../Actions/ServiceCallAction";
import {
  View,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Text,
  BackHandler
} from "react-native";
import { Container, Left, Right, Icon, Button } from "native-base";
import VideoCardItem from "../../../Components/VideoCardItem";
import { ListItem, SearchBar } from "react-native-elements"; // 0.19.1
import AppHeader from "../../../Components/AppHeader";
import { Colors } from "../../../Themes/";
import styles from "../../../Themes/styles";

class ViewAll extends Component {
  state = {
    seed: 1,
    page: 1,
    last_page: 0,
    lists: [],
    isLoading: false,
    isRefreshing: false,
    showTheSearchbar: false,
    loadMore: true,
    catId: null,
    type: null,
    input: ""
  };

  loadData = () => {
    const { lists, seed, page, last_page, isRefreshing } = this.state;
    this.setState({
      isLoading: true,
      loadMore: true
    });

    if (last_page >= page || last_page == 0) {
      // fetch(
      //   //'http://share.netispy.com/star_tube/api/v1/categories/videos?type=new&category_id=6&limit=5&page=${last_page}
      //   `http://share.netispy.com/star_tube/api/v1/categories/videos?type=new&category_id=6&limit=5&page=${page}`
      // )
      //   .then(res => res.json())
      this.props
        .viewAllVideosAPIRequest(
          this.props.languageId,
          this.state.catId,
          this.state.type,
          this.state.page
        )
        .then(() => {
          let res = this.props.viewAllVideos;

          if (res.status == "success") {
            this.setState({
              lists:
                last_page == 0 || isRefreshing
                  ? res.videos
                  : [...this.state.lists, ...res.videos],
              page: res.page,
              last_page: res.last_page,
              isRefreshing: false,
              isLoading: false
            });
          } else {
            this.setState({ isLoading: false });
          }
        })
        .catch(err => {
          console.warn(err);
        });
    }
  };

  // searchData = text => {
  //   this.setState({
  //     isLoading: true,
  //     loadMore: false,
  //     input: text,
  //     page: 1,
  //     last_page: 1,
  //     lists: []
  //   });

  //   let input = this.state.input;

  //   if (text == "") {
  //     this.loadData();
  //   } else {
  //     fetch(
  //       // http://share.netispy.com/star_tube/api/v1/categories/videos?category_id=6&type=new&page=2&limit=5
  //       `https://monkeywebstudio.com/itv3/application/index.php?r=api/employee-list&name=${text}`
  //     )
  //       .then(res => res.json())
  //       .then(res => {
  //         if (res.status) {
  //           this.setState({
  //             lists: res.data,
  //             isLoading: false
  //           });
  //         } else {
  //           this.setState({ isLoading: false });
  //         }
  //       })
  //       .catch(err => {
  //         console.error(err);
  //       });
  //   }
  // };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        last_page: 0,
        isRefreshing: true
      },
      () => {
        this.loadData();
      }
    );
  };

  handleLoadMore = () => {
    this.loadData();
  };

  componentWillMount() {
    const { params } = this.props.navigation.state;
    this.setState({ catId: params.categoryId, type: params.type });
  }
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    this.loadData();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }
  handleBackPress = () => {
    this.props.navigation.goBack(); // works best when the goBack is async
    return true;
  };

  renderHeader = () => {
    return (
      <View>
        {/* {this.state.showTheSearchbar &&
          <SearchBar
            onChangeText={text => this.searchData(text)}
            placeholder="Type Here..."
            lightTheme
            round
          />
        } */}
      </View>
    );
  };
  // <Header searchBar rounded>
  //   <Item>
  //     <Icon name="ios-search" />
  //     <Input placeholder="Search" />
  //     <Icon name="ios-people" />
  //   </Item>
  //   <Button transparent>
  //     <Text>Search</Text>
  //   </Button>
  // </Header>
  //
  renderFooter = () => {
    // console.warn(this.state.page + " last:" + this.state.last_page);

    if (!this.state.isLoading || this.state.page > this.state.last_page)
      return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator
          color={Colors.statusBarColor}
          animating
          size="large"
        />
      </View>
    );
  };

  render() {
    var that = this;
    const { lists, isRefreshing } = this.state;
    var left = (
      <Left style={{ flex: 1 }}>
        <Button onPress={() => this.handleBackPress()} transparent>
          <Icon name="ios-arrow-back" />
        </Button>
      </Left>
    );
    var right = (
      <Right style={{ flex: 1 }}>
        <Button
          onPress={() => this.props.navigation.navigate("Home")}
          transparent
        >
          <Icon name="ios-home" />
        </Button>
        <Button
          onPress={() => this.props.navigation.navigate("Search")}
          transparent
        >
          <Icon name="ios-search" />
        </Button>
      </Right>
    );
    return (
      <Container style={styles.container}>
        <AppHeader left={left} right={right} title="All Videos" />
        {lists.length == 0 && this.props.isLoading && (
          <ActivityIndicator
            size="large"
            style={styles.activityIndicator}
            color={Colors.statusBarColor}
          />
        )}

        <View style={s.scene}>
          {lists && (
            <FlatList
              horizontal={false}
              numColumns={2}
              data={lists}
              contentContainerStyle={styles.videoContainerStyle}
              renderItem={({ item: rowData }) => (
                <VideoCardItem data={rowData} isSmall={true} {...this.props} />
              )}
              ListHeaderComponent={this.renderHeader}
              ListFooterComponent={this.renderFooter}
              keyExtractor={i => i.id}
              refreshing={isRefreshing}
              onRefresh={this.handleRefresh}
              onEndReached={
                this.state.loadMore == true ? this.handleLoadMore : null
              }
              onEndThreshold={0}
            />
          )}
        </View>
      </Container>
    );
  }
}

const s = StyleSheet.create({
  scene: {
    flex: 1
  },
  list: {
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap"
  }
});

const mapDispatchToProps = dispatch => ({
  viewAllVideosAPIRequest: (language_id, category_id, type, page) =>
    dispatch(viewAllVideosAPIRequest(language_id, category_id, type, page))
});
const mapStateToProps = state => ({
  isLoading: state.serviceReducer.isLoading,
  error: state.serviceReducer.error,
  languageId: state.serviceReducer.languageId,
  viewAllVideos: state.serviceReducer.viewAllVideos
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewAll);
