import React, {
  Component
} from "react";
import SideBar from "./Sidebar";
import Splash from "./Mainscreens/Splash/index";
import Home from "./Mainscreens/Home/index";


import {
  DrawerNavigator
} from "react-navigation";

const HomeScreenRouter = DrawerNavigator({
  Home: {
    screen: Home
  },
},

  {
    contentComponent: props => < SideBar {...props} />
  }
);
export default HomeScreenRouter;