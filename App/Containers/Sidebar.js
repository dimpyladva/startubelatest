import React, { Component } from "react";
import {
  Image,
  StatusBar,
  View,
  TouchableOpacity,
  Platform,
  Share,
  Linking
} from "react-native";
import {
  Header,
  Text,
  Container,
  Content,
  Icon
} from "native-base";
// Styles
import { Images } from "../Themes";
import styles from "../Themes/styles";
import string from '../Themes/String';

export default class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeMenu: "Home"
    };
  }

  componentWillMount() {

  }

  _handlePress(screenName, params) {
    this.setState({ activeMenu: screenName });
    if (screenName == 'Share') {
      this.shareApp();
    } else if (screenName == 'Rate') {
      this.rateApp();
    } else {
      this.props.navigation.navigate(screenName, params);
    }
  }
  rateApp() {
    Linking.openURL(string.appUrl).catch(err => console.error('An error occurred', err));

  }
  shareApp() {
    Share.share({
      message: string.appUrl,
      url: string.appUrl,
      title: string.AppName
    }, {
        // Android only:
        dialogTitle: 'Share ' + string.AppName,
        // iOS only:
        excludedActivityTypes: [
          'com.apple.UIKit.activity.PostToTwitter'
        ]
      })


    // const shareOptions = {
    //   title: string.AppName,
    //   message: string.shareContent,
    //   url: string.appUrl,
    //   subject: string.AppName,//  for email,
    //   social: [Share.Social.WHATSAPP, Share.Social.FACEBOOK, Share.Social.INSTAGRAM],
    // };
    // // const shareOptions = {
    // //   title: string.AppName,
    // //   url: string.appUrl,

    // // };
    // Share.shareSingle(shareOptions);
  }
  render() {
    const { activeMenuImage } = this.state;
    StatusBar.setBarStyle("light-content", true);

    if (Platform.OS === "android") {
      StatusBar.setBackgroundColor("transparent", true);
      StatusBar.setTranslucent(true);
    }
    return (
      <Container>
        <Content style={{ backgroundColor: "#1a191f" }}>
          <View style={styles.menuContainer}>
            <Header style={styles.header}>
              <Text style={styles.headertxt}>StarTube</Text>
            </Header>
            <View style={{ backgroundColor: "grey", height: 0.3 }} />
            <View style={styles.mainview}>
              <TouchableOpacity onPress={() => this._handlePress("Home")}>
                <View style={styles.listrow}>
                  <Icon
                    name="md-home"
                    color="white"
                    style={styles.drawerIcon}
                  />

                  <Text style={styles.rowtxt}>Home</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._handlePress("Artists")}>
                <View style={styles.listrow}>
                  <Icon
                    name="ios-contact"
                    color="white"
                    style={styles.drawerIcon}
                  />

                  <Text style={styles.rowtxt}>Artists</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._handlePress("Favorites")}>
                <View style={styles.listrow}>
                  <Icon
                    name="ios-heart"
                    color="white"
                    style={styles.drawerIcon}
                  />

                  <Text style={styles.rowtxt}>My Favorite</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._handlePress("WatchLater")}>
                <View style={styles.listrow}>
                  <Icon
                    name="md-time"
                    color="white"
                    style={styles.drawerIcon}
                  />
                  <Text style={styles.rowtxt}>Watch Later</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._handlePress("Language", { first: false })}>
                <View style={styles.listrow}>
                  <Icon
                    name="md-list"
                    color="white"
                    style={styles.drawerIcon}
                  />
                  <Text style={styles.rowtxt}>Language</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._handlePress("Contact", { first: false })}>
                <View style={styles.listrow}>
                  <Icon
                    name="md-mail"
                    color="white"
                    style={styles.drawerIcon}
                  />
                  <Text style={styles.rowtxt}>Contact us</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._handlePress("About", { first: false })}>
                <View style={styles.listrow}>
                  <Icon
                    name="md-information-circle"
                    color="white"
                    style={styles.drawerIcon}
                  />
                  <Text style={styles.rowtxt}>About us</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._handlePress("Share", { first: false })}>
                <View style={styles.listrow}>
                  <Icon
                    name="md-share"
                    color="white"
                    style={styles.drawerIcon}
                  />
                  <Text style={styles.rowtxt}>Share</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._handlePress("Rate", { first: false })}>
                <View style={styles.listrow}>
                  <Icon
                    name="md-star"
                    color="white"
                    style={styles.drawerIcon}
                  />
                  <Text style={styles.rowtxt}>Rate</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
