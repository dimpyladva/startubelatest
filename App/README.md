### API integration steps 

1. Add APi => Services/Api 
2. Create const APi function => Actions/ServiceCallAction
3. add conresponding parameter in initialState and make condition in ACTION_SUCCESS =>Reducers/ServiceReducer
4. Final Add Connect with component file:
    -import { connect } from "react-redux";
    -import {name of const APi function} from '../../../Actions/ServiceCallAction';
    -   const mapDispatchToProps = (dispatch) => ({
            name of const APi function: (params) => dispatch(name of const APi function(params)),
        });
        const mapStateToProps = (state) => ({
            //CHANGE BELOW PARAMETER AS PER YOUR CODE FROM REDUCER
            isLoading: state.serviceReducer.isLoading,
            error: state.serviceReducer.error,
            tabData: state.serviceReducer.tabData
        });
        export default connect(mapStateToProps, mapDispatchToProps)(FILENAME);
    -call webservice by "this.props.$importedName()"