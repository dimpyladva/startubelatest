import { Platform, StyleSheet, Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");
import { Fonts, Metrics, Colors } from "./";
import Style from "./Style";
const itemWidth = width / 2;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.snow,
    backgroundColor: "#e0e6f2",
    paddingTop: Platform.OS == "ios" ? 20 : 0
  },
  videoCatalougeImage: {
    width: Metrics.WIDTH,
    height: Metrics.HEIGHT * 0.35,
    flex: 1,
    resizeMode: "cover",
    alignSelf: "center"
  },
  playIconContainer: {
    width: Metrics.WIDTH,
    height: "100%",
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.5)"
  },

  suggestedPeopleImgView: {
    flexDirection: "row",
    marginHorizontal: Metrics.WIDTH * 0.03,
    marginBottom: Metrics.WIDTH * 0.03
  },
  artistGrid: {
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap"
  },
  sugestedPeopleImgOne: {
    width: Metrics.WIDTH * 0.15,
    height: Metrics.WIDTH * 0.15,
    borderRadius: Metrics.WIDTH * 0.07,
    borderColor: Colors.lightGray,
    borderWidth: 0
  },
  sugestedPeopleImgTwo: {
    width: Metrics.WIDTH * 0.15,
    height: Metrics.WIDTH * 0.15,
    borderRadius: Metrics.WIDTH * 0.07,
    borderColor: Colors.lightGray,
    borderWidth: 0,
    marginLeft: -Metrics.WIDTH * 0.05
  },
  sugestedPeopleImageOne: {
    width: Metrics.WIDTH * 0.15,
    height: Metrics.WIDTH * 0.15,
    borderRadius: Metrics.WIDTH * 0.07,
    margin: 0,
    padding: 0,
    backgroundColor: Colors.hintblue,
    resizeMode: "cover",
    borderColor: Colors.navbarBackgroundColor,
    borderWidth: Metrics.WIDTH * 0.001
  },
  sugestedPeopleImageTwo: {
    width: Metrics.WIDTH * 0.15,
    height: Metrics.WIDTH * 0.15,
    resizeMode: "cover",
    borderRadius: Metrics.WIDTH * 0.07,
    margin: 0,
    padding: 0,
    backgroundColor: Colors.hintblue,
    borderColor: Colors.navbarBackgroundColor,
    borderWidth: Metrics.WIDTH * 0.001
  },
  animatedHeader: {
    position: "absolute",
    top: Platform.OS == "ios" ? 20 : 0,
    left: 0,
    right: 0,
    justifyContent: "center",
    alignItems: "center"
  },

  item: {
    backgroundColor: "#E0E0E0",
    margin: 8,
    height: 45,
    justifyContent: "center",
    alignItems: "center"
  },

  wrapper: {},
  cardView: {
    width: Style.CARD_WIDTH_RECT,
    backgroundColor: "transparent",
    alignItems: "center",
    alignContent: "center",
    flexDirection: "column",
    margin: Metrics.WIDTH * 0.015,
    shadowOffset: { width: 3, height: 3 },
    shadowColor: "#6F6F6F",
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 5
  },
  cardViewWhite: {
    width: Style.CARD_WIDTH,
    backgroundColor: Colors.white,
    //width: CARD_WIDTH_RECT,
    alignItems: "center",
    margin: Metrics.WIDTH * 0.015,
    alignItems: "flex-start",
    alignContent: "flex-start",
    flexDirection: "column",
    shadowOffset: { width: 3, height: 3 },
    shadowColor: "#6F6F6F",
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    borderRadius: 5
  },
  cardViewWhiteArtist: {
    width: Style.CARD_WIDTH_RECT * 0.9,
    backgroundColor: Colors.white,
    margin: Metrics.WIDTH * 0.02,
    alignItems: "center",
    alignContent: "center",
    flexDirection: "column",
    shadowOffset: { width: 3, height: 3 },
    shadowColor: "#6F6F6F",
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 1,
    borderRadius: 5
  },
  cardViewSmall: {
    width: Style.CARD_WIDTH_RECT,
    backgroundColor: "transparent",
    alignItems: "center",
    alignContent: "center",
    flexDirection: "column",
    padding: Metrics.WIDTH * 0.015,
    shadowOffset: { width: 3, height: 3 },
    shadowColor: "#6F6F6F",
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 5
  },
  cardContent: {
    flex: 1,
    flexDirection: "column",
    width: Style.CARD_WIDTH_RECT,
    backgroundColor: Colors.white,
    marginTop: -5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5
  },
  cardContentSmall: {
    flex: 1,
    flexDirection: "column",
    width: Style.CARD_WIDTH_RECT - Metrics.WIDTH * 0.015,
    backgroundColor: Colors.white,
    marginTop: -5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5
  },
  cardViewWidth: {
    width: Style.CARD_WIDTH_RECT_SMALL,
    borderRadius: 5
  },
  splashContainer: {
    flex: 1,
    backgroundColor: "white"
  },
  cardImage: {
    width: Style.CARD_WIDTH_RECT,
    height: Style.CARD_HEIGHT * 0.75,
    borderRadius: 5,
    borderWidth: 0,
    backgroundColor: "transparent"
  },
  cardImageSmall: {
    width: Style.CARD_WIDTH_RECT - Metrics.WIDTH * 0.015,
    height: Style.CARD_HEIGHT * 0.7,
    borderRadius: 5,
    borderWidth: 0,
    backgroundColor: "transparent"
  },
  backgroundImage: {
    width: Metrics.WIDTH,
    height: Metrics.HEIGHT,
    backgroundColor: "white",
    flex: 4,
    zIndex: 0.8,
    position: "absolute"
  },
  menuContainer: {
    height: Metrics.HEIGHT,
    backgroundColor: "#1a191f"
  },
  mainview: {
    marginLeft: 20,
    marginTop: 25
  },

  listrow: {
    backgroundColor: "transparent",
    flexDirection: "row",
    marginBottom: 25
  },
  rowicon: {
    backgroundColor: "transparent",
    tintColor: "white",
    color: "white",
    height: 20,
    width: 20,
    resizeMode: "contain",
    alignSelf: "center",
    alignItems: "center"
  },
  drawerIcon: { color: "white", fontSize: Fonts.moderateScale(22) },

  header: {
    backgroundColor: Colors.navbarBackgroundColor,
    height: Metrics.WIDTH * 0.2,
    borderBottomWidth: 0,
    elevation: 0
  },
  slide1: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#9DD6EB"
  },
  slide2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#97CAE5"
  },
  slide3: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#92BBD9"
  },

  left: {
    flex: 1
  },
  leftbtn: {
    width: 30
  },
  body: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center"
  },

  headertextTotalVideoSmall: {
    fontSize: Fonts.moderateScale(11),
    fontFamily: Fonts.type.latoBold,
    color: Colors.snow,
    paddingVertical: Metrics.WIDTH * 0.02,
    backgroundColor: Colors.navbarBackgroundColor,
    paddingHorizontal: Metrics.WIDTH * 0.04,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5
  },

  verticalLineSimple: {
    borderLeftWidth: 3,
    borderLeftColor: Colors.navbarBackgroundColor
  },
  logo10: {
    justifyContent: "center",
    alignSelf: "center",
    width: Metrics.WIDTH*0.5,
    height: Metrics.WIDTH*0.5,
    resizeMode: "contain",

  },
  gsButton: {
    backgroundColor: "#0691ce",
    height: Metrics.HEIGHT * 0.1,
    width: Metrics.WIDTH * 0.9,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    position: "absolute",
    bottom: Metrics.HEIGHT * 0.12
  },
  centerView: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  gsButtonText: {
    color: "white",
    fontSize: Fonts.moderateScale(17),
    fontFamily: Fonts.type.latoBold
  },

  right: {
    flex: 1
  },
  rightbtn: {
    width: 30,
    alignItems: "flex-end"
  },
  searchicon: {
    marginRight: 10,
    color: "#fff",
    fontSize: 24
  },
  smallIcon: {
    marginRight: 3,
    color: "#000",
    fontSize: Fonts.moderateScale(20)
  },
  smallIconColored: {
    marginRight: 3,
    color: "red",
    fontSize: Fonts.moderateScale(20)
  },

  slidesec: {
    height: Metrics.HEIGHT * 0.35,
    backgroundColor: Colors.transparent
  },

  btnsec: {
    height: Metrics.HEIGHT * 0.15,
    backgroundColor: Colors.transparent,
    borderTopWidth: 0,
    bottom: 0,
    flexDirection: "row",
    width: Metrics.WIDTH
  },

  slide: {
    flex: 1,
    backgroundColor: Colors.transparent
  },

  sliderImage: {
    resizeMode: "cover",
    height: Metrics.HEIGHT * 0.35,
    width: Metrics.WIDTH
  },
  contentStyle: {
    marginTop: 10
  },

  dot: {
    backgroundColor: Colors.white,
    width: Metrics.WIDTH * 0.015,
    height: Metrics.WIDTH * 0.015,
    borderRadius: Metrics.WIDTH * 0.012,
    marginLeft: Metrics.WIDTH * 0.005,
    marginRight: Metrics.WIDTH * 0.005
  },

  activeDot: {
    backgroundColor: Colors.navbarBackgroundColor,
    width: Metrics.WIDTH * 0.015,
    height: Metrics.WIDTH * 0.015,
    borderRadius: Metrics.WIDTH * 0.012,
    marginLeft: Metrics.WIDTH * 0.005,
    marginRight: Metrics.WIDTH * 0.005
  },

  discoverBg: {
    backgroundColor: "#0691ce",
    alignSelf: "center",
    justifyContent: "center",
    paddingLeft: Metrics.WIDTH * 0.08,
    paddingRight: Metrics.WIDTH * 0.08,
    paddingTop: Metrics.WIDTH * 0.02,
    paddingBottom: Metrics.WIDTH * 0.02,
    borderRadius: Metrics.WIDTH * 0.06,
    marginLeft: Metrics.WIDTH * 0.35
  },

  //circular view Style

  connectionBg: {
    marginVertical: Metrics.WIDTH * 0.01,
    width: Metrics.WIDTH,
    alignSelf: "center"
  },
  detailPageBg: {
    marginTop: Metrics.WIDTH * 0.025,
    flex: 1,
    paddingHorizontal: Metrics.WIDTH * 0.025,

    backgroundColor: Colors.transparent
  },
  connectionHeaderBg: {
    marginHorizontal: Metrics.WIDTH * 0.03,
    marginVertical: Metrics.WIDTH * 0.03,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  connectionHeaderBgRow: {
    flexDirection: "row"
  },
  verticalLine: {
    borderLeftWidth: 5,
    marginRight: 10,
    borderLeftColor: Colors.tabBarBackgroundColor,
    height: Metrics.HEIGHT * 0.05
  },
  connectionPhotosTxt: {
    fontSize: Fonts.moderateScale(14),
    fontFamily: Fonts.type.latoBold,
    color: Colors.blacktxt,
    textAlignVertical: "center"
  },
  connectionPhotosCountTxt: {
    backgroundColor: Colors.tabBarBackgroundColor,
    color: "white",
    fontSize: Fonts.moderateScale(12),
    fontFamily: Fonts.type.latoRegular,
    borderRadius: 10,
    paddingHorizontal: 15,
    paddingVertical: 5
  },
  horizontalRow: {
    paddingLeft: Metrics.WIDTH * 0.03,
    alignSelf: "flex-end",
    justifyContent: "flex-end",
    flexDirection: "row",
    marginRight: 0,
    backgroundColor: Colors.white
  },

  seeMoreStyle: {
    color: Colors.navbarBackgroundColor,
    fontSize: Fonts.moderateScale(11),
    marginRight: 10,
    fontFamily: Fonts.type.latoRegular
  },
  connectionProfileBg: {
    flexDirection: "row",
    marginLeft: Metrics.WIDTH * 0.015,
    marginRight: Metrics.WIDTH * 0.015,
    marginBottom: Metrics.WIDTH * 0.04
  },
  marginLow: {
    margin: Metrics.WIDTH * 0.01,
    justifyContent: "center",
    alignItems: "center"
  },
  connectionProfileSocialBg: {
    width: Metrics.WIDTH * 0.176,
    flexDirection: "row"
  },
  // width: Metrics.WIDTH * 0.100,
  // height: Metrics.WIDTH * 0.100,
  // borderRadius: Metrics.WIDTH * 0.50,

  artistImage: {
    width: Style.CARD_WIDTH_RECT * 0.5,
    height: Style.CARD_WIDTH_RECT * 0.5,
    borderRadius: Metrics.WIDTH * 0.15,
    resizeMode: "cover",
    alignSelf: "center",
    marginVertical: Metrics.WIDTH * 0.01
  },
  artistImageDetail: {
    width: Style.CARD_WIDTH_RECT * 0.7,
    height: Style.CARD_WIDTH_RECT * 0.7,
    borderRadius: Metrics.WIDTH * 0.2,
    marginVertical: Metrics.WIDTH * 0.01,
    resizeMode: "cover",
    alignSelf: "center"
  },
  connectionSocialIconBg: {
    width: Metrics.WIDTH * 0.04,
    height: Metrics.WIDTH * 0.04,
    alignSelf: "flex-end",
    marginLeft: -(Metrics.WIDTH * 0.03),
    borderRadius: Metrics.WIDTH * 0.02,
    borderWidth: 1,
    borderColor: Colors.snow,
    justifyContent: "center",
    alignItems: "center"
  },
  connectionProfileSocialBg: {
    width: Metrics.WIDTH * 0.176,
    flexDirection: "row"
  },
  connectionUserImgContainer: {
    shadowOffset: { width: 3, height: 3 },
    shadowColor: "#6F6F6F",
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 5
  },
  connectionUserImg: {
    width: Metrics.WIDTH * 0.16,
    height: Metrics.WIDTH * 0.16,
    borderRadius: Metrics.WIDTH * 0.08,
    resizeMode: "cover"
  },
  //tab
  tabNameView: {
    flexDirection: "row",
    width: 70,
    justifyContent: "space-between"
  },

  indicator: {
    backgroundColor: "#0691ce",
    width: Metrics.WIDTH * 0.15,
    marginLeft: Metrics.WIDTH * 0.055
  },

  tabBg: {
    backgroundColor: Colors.transparent
  },
  main: {
    width: Metrics.WIDTH,
    flex: 1,
    backgroundColor: "#e0e6f2",
    flexDirection: "column"
  },

  profileImg: {
    width: Metrics.WIDTH * 0.1,
    height: Metrics.WIDTH * 0.1,
    borderRadius: Metrics.WIDTH * 0.05
  },

  viewOverlay: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "rgba(30, 42, 54, 0.6)"
  },

  viewBorder: {
    position: "absolute",
    top: "10%",
    left: "5%",
    right: "5%",
    bottom: "10%",
    borderWidth: 1,
    borderColor: "rgba(253, 253, 253, 0.2)"
  },

  mainRow: {
    flexDirection: "row",
    margin: Metrics.HEIGHT * 0.015,
    alignItems: "center"
  },
  nameDesignationMainView: {
    marginLeft: Metrics.WIDTH * 0.045,
    flexDirection: "column"
  },

  nameDesignationView: {
    flexDirection: "column",
    justifyContent: "center"
  },

  dividerHorizontal: {
    width: Metrics.WIDTH * 0.95,
    height: Metrics.HEIGHT * 0.001,
    backgroundColor: "#e6e6e6",
    justifyContent: "center",
    alignSelf: "center"
  },
  dividerHorizontalColored: {
    width: Metrics.WIDTH * 0.95,
    height: Metrics.HEIGHT * 0.001,
    backgroundColor: Colors.navbarBackgroundColor,
    justifyContent: "center",
    alignSelf: "center"
  },
  // dividerHorizontal: {
  //   backgroundColor: "#ebebeb",
  //   height: 1,
  //   width: Metrics.WIDTH * 0.94
  // },
  cardStyle: {
    padding: Fonts.moderateScale(10),
    borderRadius: Fonts.moderateScale(5)
  },
  tabUnderLine: {
    backgroundColor: Colors.tabUnderLineColor,
    height: 3
  },
  tabUnderLineTrans: {
    backgroundColor: "transparent",
    height: 2
  },
  tabText: {
    fontSize: Fonts.moderateScale(12),
    fontFamily: Fonts.type.latoBold
  },
  itemText: {
    color: "black",
    fontSize: 16
  },
  headerText: {
    color: "white",
    fontSize: 22
  },
  headertxt: {
    paddingVertical: Fonts.moderateScale(20),
    fontSize: 27,
    textAlign: "center",
    color: "white",
    fontFamily: Platform.OS === "ios" ? null : "Steelworks_Vintage_Demo"
  },
  rowtxt: {
    color: "white",
    fontSize: Fonts.moderateScale(16),
    backgroundColor: "transparent",
    marginLeft: 20,
    textAlign: "center"
  },
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold"
  },
  textTitle: {
    color: Colors.snow,
    fontSize: Fonts.moderateScale(16),
    marginTop: 5,
    textAlignVertical: "center",
    textAlign: "center",
    fontFamily: Fonts.type.latoBold
  },
  title: {
    color: "#fff",
    fontSize: Fonts.moderateScale(15),
    fontFamily: Fonts.type.latoBold,
    textAlign: "center",
    fontWeight: "bold"
  },
  headertextSmall: {
    fontSize: Fonts.moderateScale(12),
    fontFamily: Fonts.type.latoSemiRegular,
    color: Colors.darkText,
    paddingVertical: Metrics.WIDTH * 0.02
  },
  headertext: {
    fontFamily: Fonts.type.latoLight,
    textAlign: "center",
    alignSelf: "center",
    fontSize: Fonts.moderateScale(21),
    width: Metrics.WIDTH * 0.85,
    color: "#363636"
  },

  desctext: {
    fontFamily: Fonts.type.latoRegular,
    textAlign: "center",
    alignSelf: "center",
    fontSize: Fonts.moderateScale(11),
    width: Metrics.WIDTH * 0.7,
    color: "#6f6f6f",
    marginTop: 20
  },
  discoverTxt: {
    fontFamily: Fonts.type.latoMedium,
    fontSize: Fonts.moderateScale(12),
    color: Colors.snow,
    textAlign: "center"
  },

  skipTxt: {
    fontFamily: Fonts.type.latoMedium,
    fontSize: Fonts.moderateScale(16),
    color: "#6f6f6f",
    textAlign: "right",
    marginRight: Metrics.WIDTH * 0.08
  },

  searchSuggestionNameTxt: {
    fontSize: Fonts.moderateScale(12),
    fontFamily: Fonts.type.latoBold,
    color: Colors.blacktxt
  },
  searchSuggestionTypeTxt: {
    fontSize: Fonts.moderateScale(10),
    fontFamily: Fonts.type.latoBold
  },
  moreContainer: {
    margin: 10,
    flex: 1,
    flexDirection: "row"
  },
  nameTitle: {
    color: Colors.darkText,
    fontSize: Fonts.moderateScale(14),
    fontFamily: Fonts.type.latoRegular,
    width: Style.CARD_WIDTH_RECT * 0.8,
    textAlign: "left"
  },
  detailNameTitle: {
    color: Colors.darkText,
    fontSize: Fonts.moderateScale(14),
    fontFamily: Fonts.type.latoRegular,
    textAlign: "left",
    paddingVertical: Metrics.WIDTH * 0.045,
    paddingHorizontal: Metrics.WIDTH * 0.01
  },
  moreIcon: {
    fontSize: Fonts.moderateScale(20),
    color: Colors.navbarBackgroundColor,
    width: Style.CARD_WIDTH_RECT * 0.2,
    justifyContent: "center",
    alignSelf: "center",
    paddingVertical: 10
  },

  label: {
    fontSize: Fonts.moderateScale(12),
    fontFamily: Fonts.type.latoRegular,
    color: "#0691ce",
    paddingLeft: Metrics.WIDTH * 0.045
  },

  normalLabel: {
    fontSize: Fonts.moderateScale(12),
    fontFamily: Fonts.type.latoRegular,
    color: "#d2d2d2",
    paddingLeft: Metrics.WIDTH * 0.045
  },
  nameTxt: {
    flex: 0.95,
    padding: 5,
    fontSize: 12,
    alignSelf: "flex-start",
    fontFamily: Fonts.type.latoRegular,
    textAlign: "center",
    width: "100%",
    color: Colors.darkText
    // fontSize: Fonts.moderateScale(12),
    // fontFamily: Fonts.type.latoBold,
    // textAlign: "left",
    // paddingTop: 2,
    // paddingLeft: 3,
    // paddingRight: 2,
    // paddingBottom: 1,
    // alignSelf: "flex-start"
  },
  homeCategoryText: {
    height: Style.CARD_HEIGHT, // 50% of screen height
    width: "100%",
    position: "absolute",
    alignItems: "center",
    justifyContent: "center"
  },
  artistDetailHeaderText: {
    height: Style.CARD_HEIGHT, // // 50% of screen height
    width: "100%",
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(30, 42, 54, 0.6)"
  },
  viewHeaderTitle: {
    textAlign: "center",
    color: "#fdfdfd",
    fontSize: Style.FONT_SIZE_TITLE
  },
  detailPageTextTitle: {
    color: Colors.white,
    fontSize: Fonts.moderateScale(12),
    marginTop: Metrics.HEIGHT * 0.002,
    fontFamily: Fonts.type.latoMedium
  },
  designationTxt: {
    color: Colors.txtgrey,
    fontSize: Fonts.moderateScale(12),
    marginTop: Metrics.HEIGHT * 0.002,
    textAlign: "left",
    fontFamily: Fonts.type.latoRegular
  },
  checkboxContainerStyle: {
    marginTop: Metrics.HEIGHT * 0.02
  },
  videoContainerStyle: {
    padding: Metrics.HEIGHT * 0.008
  },
  activityIndicator: {
    alignSelf: "center",
    justifyContent: "center",
    marginVertical: Metrics.HEIGHT * 0.1,
    flex: 1,
    color: Colors.navbarBackgroundColor
  },
  imageActivityIndicator: {
    color: Colors.navbarBackgroundColor
  },
  tabButton: {
    alignItems: "center",
    flex: 1,
    height: Metrics.HEIGHT * 0.07,
    borderRadius: 0,
    backgroundColor: Colors.tabBarBackgroundColor,
    borderBottomColor: Colors.transparent,
    borderBottomWidth: 3
  },
  tabButtonActive: {
    alignItems: "center",
    flex: 1,
    borderRadius: 0,
    height: Metrics.HEIGHT * 0.07,
    backgroundColor: Colors.tabBarBackgroundColor,
    borderBottomColor: Colors.tabUnderLineColor,
    borderBottomWidth: 3
  },
  tabBarInactiveText: {
    textAlign: "center",
    alignSelf: "center",
    fontFamily: Fonts.type.latoBold,
    color: Colors.tabBarInactiveTextColor
  },
  tabBarActiveText: {
    color: Colors.tabBarActiveTextColor,
    textAlign: "center",
    fontFamily: Fonts.type.latoBold,
    alignSelf: "center"
  },
  contactHeading: {
    fontSize: Fonts.moderateScale(16),
    marginTop: 5,
    textAlignVertical: "center",
    textAlign: "center",
    fontFamily: Fonts.type.latoBold
  },
  contactHeader: {
    fontSize: Fonts.moderateScale(16),
    marginTop: 5,
    textAlignVertical: "center",
    textAlign: "left",
    fontFamily: Fonts.type.latoBold
  },
  containerMessage:{
    fontSize: Fonts.moderateScale(16),
    marginTop: 25,
    textAlignVertical: "center",
    textAlign: "center",
    fontFamily: Fonts.type.latoBold
  }
});

export default styles;
