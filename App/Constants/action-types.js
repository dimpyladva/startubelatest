export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const SERVICE_PENDING = 'service_pending';
export const SERVICE_ERROR = 'service_error';
export const SERVICE_SUCCESS = 'service_success';