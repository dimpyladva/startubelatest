import * as ActionTypes from './ActionTypes';
// import { connect } from 'react-redux';
import API from '../Services/Api';
import apisauce from "apisauce";
let searchSuggestionCancel;

const api = API.create();
export const updateInternetStatusAction = isConnected => ({
    type: ActionTypes.UPDATE_INTERNET_STATUS,
    data: isConnected
});
export const getGeneralDetails = () => (
    (dispatch) => {
        dispatch(serviceActionPending());
        return api.generalDetailsAPiRequest()
            .then((response) => dispatch(serviceActionSuccess('generalDetails', response.data)))
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const login = (device_id, device_type) => (
    (dispatch) => {
        dispatch(serviceActionPending());
        return api.loginAPIRequest(device_id, device_type)
            .then((response) => dispatch(serviceActionSuccess('user', response.data)))
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const searchSuggest = (keyword, languageId) => (
    (dispatch) => {
        if (searchSuggestionCancel) {
            searchSuggestionCancel();
        }
        const CancelToken = apisauce.CancelToken;
        const cancelTokenExecutor = new CancelToken(function executor(c) {
            searchSuggestionCancel = c;
        })
        dispatch(serviceActionPending());
        return api.searchSuggestReq(keyword, languageId, cancelTokenExecutor)
            .then((response) => {
                if (response.problem != "CANCEL_ERROR") {
                    dispatch(serviceActionSuccess('searchSuggestions', response.data))
                }
            })
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const search = (keyword, languageId) => (
    (dispatch) => {
        dispatch(serviceActionPending());
        return api.searchReq(keyword, languageId)
            .then((response) => dispatch(serviceActionSuccess('search', response.data)))
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const searchVideo = (keyword, languageId, page, isRefreshOnly = false) => (
    (dispatch) => {
        if (isRefreshOnly) {
            dispatch(serviceSearchRefreshVideoActionPending());
        }
        else {
            dispatch(serviceSearchVideoActionPending())
        }
        return api.searchVideoReq(keyword, languageId, page)
            .then((response) => dispatch(serviceActionSuccess('searchVideo', response.data)))
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const searchArtist = (keyword, languageId, page, isRefreshOnly = false) => (
    (dispatch) => {
        if (isRefreshOnly) {
            dispatch(serviceSearchRefreshArtistActionPending());
        }
        else {
            dispatch(serviceSearchArtistActionPending())
        }
        return api.searchArtistsReq(keyword, languageId, page)
            .then((response) => dispatch(serviceActionSuccess('searchArtist', response.data)))
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const getArtists = (languageId) => ((dispatch) => {

    dispatch(serviceActionPending());
    return api.artistAPiRequest(languageId)
        .then((response) => dispatch(serviceActionSuccess('artists', response.data.artists)))
        .catch(error => {
            dispatch(serviceActionError(error))
        });

});
export const getRoleArtists = (languageId, artist_role_id, page) => (
    (dispatch) => {
        dispatch(serviceActionPending());
        return api.roleArtistsAPiRequest(languageId, artist_role_id, page)
            .then((response) => {
                dispatch(serviceActionSuccess('roleArtists', response.data))
            })
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const getArtistDetails = (languageId, artist_id, page) => (
    (dispatch) => {
        dispatch(serviceActionPending());
        return api.artistDetailAPIRequest(languageId, artist_id, page)
            .then((response) => {
                dispatch(serviceActionSuccess('artistdetails', response.data))
            })
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const getCategories = (languageId) => (
    (dispatch) => {
        dispatch(serviceActionPending());
        return api.categoryAPIRequest(languageId)
            .then((response) => dispatch(serviceActionSuccess('categories', response.data)))
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const tabViewData = (categoryId, languageId) => (
    (dispatch) => {
        var extraData = {};
        extraData.categoryId = categoryId;
        dispatch(tabActionPending());
        return api.tabVideosAPIRequest(categoryId, languageId)
            .then((response) => {

                dispatch(serviceActionSuccess('tabData', response.data, extraData));
                dispatch(tabActionSuccess());
            })
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const homeDataRequest = (language_id) => (
    (dispatch) => {

        dispatch(tabActionPending());
        return api.homeAPiRequest(language_id)
            .then((response) => {

                dispatch(serviceActionSuccess('homeData', response.data));
                dispatch(tabActionSuccess());
            })
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const videoDetail = (language_id, video_id, user_id, isRefreshOnly) => (
    (dispatch) => {
        if (!isRefreshOnly) {
            dispatch(serviceActionPending());
        }
        return api.videoDetailAPIRequest(language_id, video_id, user_id)
            .then((response) => dispatch(serviceActionSuccess('videoDetail', response.data)))
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const viewAllVideosAPIRequest = (language_id, category_id, type, page) => (
    (dispatch) => {
        dispatch(serviceActionPending());
        return api.viewAllVideosAPIRequest(language_id, category_id, type, page)
            .then((response) => dispatch(serviceActionSuccess('viewAllVideos', response.data)))
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const myFavVideosAPIRequest = (language_id, user_id, page) => (
    (dispatch) => {
        dispatch(serviceActionPending());
        return api.myFavsAPIRequest(language_id, user_id, page)
            .then((response) => dispatch(serviceActionSuccess('myFavVideos', response.data)))
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const watchLaterVideosAPIRequest = (language_id, user_id, page) => (
    (dispatch) => {
        dispatch(serviceActionPending());
        return api.watchLatersAPIRequest(language_id, user_id, page)
            .then((response) => dispatch(serviceActionSuccess('watchLaterVideos', response.data)))
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const addLikeRequest = (video_id, user_id) => (
    (dispatch) => {

        dispatch(serviceAddActionPendig());
        return api.addLike(video_id, user_id)
            .then((response) => dispatch(serviceActionSuccess('addlike', response.data)))
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const addWatchLaterRequest = (video_id, user_id) => (
    (dispatch) => {
        dispatch(serviceAddActionPendig());

        return api.addWatchLater(video_id, user_id)
            .then((response) => {
                dispatch(serviceActionSuccess('addWatchLater', response.data))
            })
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const setLanguage = (languageId) => (

    (dispatch) => {
        let extraData = {};
        extraData.languageId = languageId;
        dispatch(serviceActionPending());
        return api.languageConstants(languageId)
            .then((response) => dispatch(serviceActionSuccess('languageConstants', response.data, extraData)))
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);

export const getlanguageList = () => (
    (dispatch) => {
        dispatch(serviceActionPending());
        return api.languageList()
            .then((response) => dispatch(serviceActionSuccess('languageList', response.data)))
            .catch(error => {
                dispatch(serviceActionError(error))
            });
    }
);
export const setOrientationChange = (orientation) => (
    (dispatch) => {
        // console.log(orientation)
        return dispatch(serviceActionSuccess('orientationDidChange', { 'orientation': orientation }));
    }
);
// export const getLabels = (language_id) => (
//     (dispatch) => {
//         dispatch(serviceActionPending());
//         return api.languageLabels(language_id)
//             .then((response) => dispatch(serviceActionSuccess('labels', response.data)))
//             .catch(error => {
//                 dispatch(serviceActionError(error))
//             });
//     }
// );

export const serviceActionPending = () => ({
    type: ActionTypes.SERVICE_PENDING
})
export const serviceSearchVideoActionPending = () => ({
    type: ActionTypes.SERVICE_SEARCH_VIDEO_PENDING
})
export const serviceSearchArtistActionPending = () => ({
    type: ActionTypes.SERVICE_SEARCH_ARTIST_PENDING
})
export const serviceSearchRefreshVideoActionPending = () => ({
    type: ActionTypes.SERVICE_SEARCH_REFRESH_VIDEO_PENDING
})
export const serviceSearchRefreshArtistActionPending = () => ({
    type: ActionTypes.SERVICE_SEARCH_REFRESH_ARTIST_PENDING
})
export const serviceAddActionPendig = () => ({
    type: ActionTypes.ADD_ACTION_PENDING
})
export const serviceActionError = (error) => ({
    type: ActionTypes.SERVICE_ERROR,
    error: error
})

export const serviceActionSuccess = (dataType, data, extraData = {}) => ({
    type: ActionTypes.SERVICE_SUCCESS,
    dataType: dataType,
    data: data,
    extraData: extraData
})
export const tabActionPending = () => ({
    type: ActionTypes.TAB_PENDING
})
export const tabActionSuccess = () => ({
    type: ActionTypes.TAB_SUCCESS
})
export const setLanguageSuccess = (languageId) => ({
    type: ActionTypes.SET_LANGUAGE,
    languageId: languageId
})