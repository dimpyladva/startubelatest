// Simple React Native specific changes

import "../I18n/I18n";

export default {
  // font scaling override - RN default is on
  allowTextFontScaling: true,
  apiURL: "http://aidtpa.com/startube/api/v1/"
  //apiURL : "http://share.netispy.com/star_tube/api/v1/"
};
