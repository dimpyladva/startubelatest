import apisauce from 'apisauce';
import AppConfig from '../Config/AppConfig';

const create = (baseURL = AppConfig.apiURL) => {

  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache'
    },
    // 10 second timeout...
    timeout: 10000
  })


  const loginAPIRequest = (device_id, device_type) => api.post('/users/login', { device_id: device_id, type: device_type })
  const categoryAPIRequest = (languageId) => api.get('categories/list', { 'language_id': languageId })
  const tabVideosAPIRequest = (category_id, language_id) => api.get('categories/videos_list?', { 'category_id': category_id, 'language_id': language_id })
  const videoDetailAPIRequest = (language_id, video_id, user_id) => api.get('videos/detail', { 'language_id': language_id, 'video_id': video_id, 'user_id': user_id })
  const addLike = (video_id, user_id) => api.post('videos/like', { video_id, user_id })
  const addWatchLater = (video_id, user_id) => api.post('videos/view_later', { video_id, user_id })
  const languageList = () => api.get('languages/list')
  const languageConstants = (language_id) => api.get('languages/language_constant', { 'language_id': language_id })
  const viewAllVideosAPIRequest = (language_id, category_id, type, page) => api.get('categories/videos', { 'language_id': language_id, 'category_id': category_id, 'type': type, 'page': page, 'limit': 10 })
  const myFavsAPIRequest = (language_id, user_id, page) => api.get('videos/user_like_list', { 'language_id': language_id, 'user_id': user_id, 'page': page, 'limit': 10 })
  const watchLatersAPIRequest = (language_id, user_id, page) => api.get('videos/user_view_later_list', { 'language_id': language_id, 'user_id': user_id, 'page': page, 'limit': 10 })
  const artistDetailAPIRequest = (language_id, artist_id, page) => api.get('artists/detail', { 'language_id': language_id, 'artist_id': artist_id, 'page': page, 'limit': 10 })
  const artistAPiRequest = (language_id) => api.get('artists/all', { 'language_id': language_id })
  const roleArtistsAPiRequest = (language_id,artist_role_id,page) => api.get('artists/role_artists', { 'language_id': language_id,'artist_role_id':artist_role_id,'page':page })
  const homeAPiRequest = (language_id) => api.get('home/all', { 'language_id': language_id })
  const generalDetailsAPiRequest = () => api.get('general_details/details')
  const searchSuggestReq = (Keyword, language_id,cancelToken) => {
    return api.get('search/suggestion', { 'keyword': Keyword, 'language_id': language_id },{
      cancelToken
    })
  }
  //const searchReq = (Keyword, language_id) => api.get('search/search', { 'keyword': Keyword, 'language_id': language_id })
  const searchVideoReq = (Keyword, language_id, page) => api.get('search/search_videos', { 'keyword': Keyword, 'language_id': language_id, 'page': page })
  const searchArtistsReq = (Keyword, language_id, page) => api.get('search/search_artists', { 'keyword': Keyword, 'language_id': language_id, 'page': page })

  return {
    //a list of the API functions from step 2
    loginAPIRequest,
    categoryAPIRequest,
    tabVideosAPIRequest,
    videoDetailAPIRequest,
    addLike,
    addWatchLater,
    languageList,
    languageConstants,
    viewAllVideosAPIRequest,
    artistDetailAPIRequest,
    artistAPiRequest,
    myFavsAPIRequest,
    watchLatersAPIRequest,
    homeAPiRequest,
    searchSuggestReq,
    // searchReq,
    searchVideoReq,
    searchArtistsReq,
    generalDetailsAPiRequest,
    roleArtistsAPiRequest
  }
}

// let's return back our create method as the default.
export default {
  create
}
