import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist';
import { navReducer } from '../Navigation/ReduxNavigation';
import serviceReducer from './ServiceReducer';
import ReduxPersist from '../Config/ReduxPersist';
export const reducers = combineReducers({
    serviceReducer,
    nav: navReducer
})
let rootReducer = reducers
if (ReduxPersist.active) {
    const persistConfig = ReduxPersist.storeConfig
    rootReducer = persistReducer(persistConfig, reducers)
}
export default rootReducer;