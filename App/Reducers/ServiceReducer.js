import * as Actions from "../Actions/ActionTypes";

const initialState = {
  isConnected: false,
  isLoading: false,
  isActionLoading: false,
  isTabLoading: false,
  userLoggedIn: false,
  refreshVideoDetail: false,
  error: undefined,
  data: {},
  user: {},
  categories: [],
  videoDetailData: {},
  languageList: [],
  languageId: null,
  languageConstants: {},
  viewAllVideos: {},
  addlike: {},
  addWatchLater: {},
  artistDetail: {},
  artists: [],
  favorites: {},
  watchLaters: {},
  homeData: {},
  searchSuggestions: [],
  searchedData: {},
  isSearchVideoEnd: false,
  isSearchRefreshVideoLoading: false,
  isSearchVideoLoading: false,
  searchVideos: [],
  searchVideosPage: 1,
  isSearchArtistEnd: false,
  isSearchRefreshArtistLoading: false,
  isSearchArtistLoading: false,
  searchArtists: [],
  searchArtistsPage: 1,
  isWatchLaterUpdated: false,
  isFavouriteUpdated: false,
  currentOrientation: "",
  orientationChanged: false,
  roleArtists: {},
  generalDetails: {}
};

const ServiceReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.TAB_PENDING:
      return Object.assign({}, state, {
        isTabLoading: true
      });
    case Actions.ADD_ACTION_PENDING:
      return Object.assign({}, state, {
        addWatchLater: {},
        addlike: {},
        isActionLoading: true
      });
    case Actions.TAB_SUCCESS:
      return Object.assign({}, state, {
        isTabLoading: false
      });
    case Actions.SERVICE_PENDING:
      return Object.assign({}, state, {
        isLoading: true,
        searchSuggestions: []
      });
    case Actions.SERVICE_ERROR:
      return Object.assign({}, state, {
        isLoading: false,
        error: action.error
      });
    case Actions.SERVICE_SUCCESS:
      let response = {
        isLoading: false,
        userLoggedIn: true
      };
      if (action.dataType == "user") {
        response.user = action.data;
      } else if (action.dataType == "generalDetails") {
        response.generalDetails = action.data;
      } else if (action.dataType == "categories") {
        response.categories = action.data.categories;
      } else if (action.dataType == "tabData") {
        response.categories = state.categories.map((category, categoryId) => {
          if (action.extraData.categoryId !== category.id) return category;
          return { ...category, tabData: action.data };
        });
      } else if (action.dataType == "videoDetail") {
        response.videoDetailData = action.data;
        response.refreshVideoDetail = false;
      } else if (action.dataType == "languageConstants") {
        response.languageConstants = action.data.labels;
        response.languageId = action.extraData.languageId;
      } else if (action.dataType == "languageList") {
        response.languageList = action.data.languages;
      } else if (action.dataType == "viewAllVideos") {
        response.viewAllVideos = action.data;
      } else if (action.dataType == "addlike") {
        response.addlike = action.data;
        response.refreshVideoDetail = true;
        response.isFavouriteUpdated = true;
        response.isActionLoading = false;
      } else if (action.dataType == "addWatchLater") {
        response.addWatchLater = action.data;
        response.refreshVideoDetail = true;
        response.isWatchLaterUpdated = true;
        response.isActionLoading = false;
      } else if (action.dataType == "artistdetails") {
        response.artistDetail = action.data;
      } else if (action.dataType == "roleArtists") {
        response.roleArtists = action.data;
      } else if (action.dataType == "artists") {
        response.artists = action.data;
      } else if (action.dataType == "myFavVideos") {
        response.favorites = action.data;
        response.isFavouriteUpdated = false;
      } else if (action.dataType == "watchLaterVideos") {
        response.watchLaters = action.data;
        response.isWatchLaterUpdated = false;
      } else if (action.dataType == "homeData") {
        response.homeData = action.data;
      } else if (action.dataType == "searchSuggestions") {
        response.searchSuggestions = action.data.sugestions;
      } else if (action.dataType == "search") {
        response.searchedData = action.data;
      } else if (action.dataType == "searchVideo") {
        response.searchVideosPage = action.data.page;
        response.searchVideos =
          response.searchVideosPage == 2
            ? action.data.videos
            : [...state.searchVideos, ...action.data.videos];
        response.isSearchVideoEnd = action.data.videos.length == 0;
        response.isSearchVideoLoading = false;
        response.isSearchRefreshVideoLoading = false;
      } else if (action.dataType == "searchArtist") {
        response.searchArtists =
          response.searchArtistsPage > 2
            ? action.data.artists.length > 0
              ? [...state.searchArtists, ...action.data.artists]
              : state.searchArtists
            : action.data.artists;
        response.searchArtistsPage = action.data.page;
        response.isSearchArtistLoading = false;
        response.isSearchArtistEnd = action.data.artists.length == 0;
        response.isSearchRefreshArtistLoading = false;
      } else if (action.dataType == "orientationDidChange") {
        response.currentOrientation = action.data.orientation;
        response.orientationChanged =
          state.currentOrientation != action.data.orientation;
      } else {
        response.data = action.data;
      }
      return Object.assign({}, state, response);
    case Actions.SERVICE_SEARCH_ARTIST_PENDING:
      return Object.assign({}, state, {
        isSearchArtistLoading: true
      });
    case Actions.SERVICE_SEARCH_VIDEO_PENDING:
      return Object.assign({}, state, {
        isSearchVideoLoading: true
      });
    case Actions.SERVICE_SEARCH_REFRESH_ARTIST_PENDING:
      return Object.assign({}, state, {
        isSearchRefreshArtistLoading: true
      });
    case Actions.SERVICE_SEARCH_REFRESH_VIDEO_PENDING:
      return Object.assign({}, state, {
        isSearchRefreshVideoLoading: true
      });
    case Actions.UPDATE_INTERNET_STATUS:
      return Object.assign({}, state, {
        isConnected: action.data
      });
    default:
      return state;
  }
};

export default ServiceReducer;
