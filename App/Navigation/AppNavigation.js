import React from "react";
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createDrawerNavigator } from 'react-navigation-drawer'
import ViewAll from "../Containers/Mainscreens/ViewAll/index";
import VideoDetail from "../Containers/Mainscreens/VideoDetail/index";
import Splash from "../Containers/Mainscreens/Splash";
import styles from "./Styles/NavigationStyles";
import Home from "../Containers/Mainscreens/Home/index";
import Language from "../Containers/Mainscreens/Language/index";
import SideBar from "../Containers/Sidebar";
import ArtistDetail from "../Containers/Mainscreens/ArtistDetail/index";
import Artists from "../Containers/Mainscreens/Artists/index";
import ArtistList from "../Containers/Mainscreens/ArtistList/index";
import Favorites from "../Containers/Mainscreens/Favorites/index";
import WatchLater from "../Containers/Mainscreens/WatchLater/index";
import Search from "../Containers/Mainscreens/Search/index";
import FullScreenVideo from "../Containers/Mainscreens/VideoDetail/FullScreenVideo";
import Contact from "../Containers/Mainscreens/Contact";
import About from "../Containers/Mainscreens/About";
import { Metrics } from "../Themes/";

const LaunchScreen = createDrawerNavigator(
  {
    Language: {
      screen: Language
    },
    Home: {
      screen: Home
    },
    Artists: {
      screen: Artists
    },
    Favorites: {
      screen: Favorites
    },
    WatchLater: {
      screen: WatchLater
    },
    About: {
      screen: About
    },
    Contact: {
      screen: Contact
    },
  },
  {
    contentComponent: props => <SideBar {...props} />,
    // initialRouteName: "Home",
  }
);
const PrimaryNav = createStackNavigator(
  {
    SplashScreen: { screen: Splash },
    LaunchScreen: { screen: LaunchScreen },
    VideoDetail: { screen: VideoDetail },
    ViewAll: { screen: ViewAll },
    ArtistDetail: { screen: ArtistDetail },
    ArtistList: { screen: ArtistList },
    Search: { screen: Search },
    FullScreenVideo: { screen: FullScreenVideo }
  },
  {
    // Default config for all screens
    headerMode: "none",
    initialRouteName: "SplashScreen",
    // navigationOptions: {
    //   headerStyle: styles.header
    // }
  }
);
export default createAppContainer(PrimaryNav)

