// import React from 'react'
// import { addNavigationHelpers } from 'react-navigation'
// import { connect } from 'react-redux'
// import AppNavigation from './AppNavigation'

// // here is our redux-aware smart component
// function ReduxNavigation(props) {
//   return <AppNavigation navigation={addNavigationHelpers({
//     dispatch: props.dispatch,
//     state: props.nav,
//   })} />
// }

// const mapStateToProps = state => ({ nav: state.nav })
// export default connect(mapStateToProps)(ReduxNavigation)



//--------MANAN CODE--------//
import { connect } from 'react-redux';
import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
  createNavigationReducer
} from 'react-navigation-redux-helpers';
import AppNavigation from './AppNavigation';


export const navReducer = createNavigationReducer(AppNavigation);

export const middlewareReactNavigation = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
);

const ReduxNavigation = reduxifyNavigator(AppNavigation, "root");
const mapStateToProps = (state) => ({
  state: state.nav,
});
export const AppWithNavigationState = connect(mapStateToProps)(ReduxNavigation);
