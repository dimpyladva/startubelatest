import { StackNavigator } from 'react-navigation'
import LaunchScreen from '../Containers/LaunchScreen'
import Splash from "../Containers/Mainscreens/Splash";
import styles from './Styles/NavigationStyles'
import Language from "../Containers/Mainscreens/Language/index";
import ViewAll from "../Containers/Mainscreens/ViewAll/index";
import VideoDetail from "../Containers/Mainscreens/VideoDetail/index";
// const DrawerNavigation = StackNavigator({
//   DrawerStack: { screen: LaunchScreen }
// }, {
//   // Default config for all screens
//   headerMode: 'none',
//   navigationOptions: {
//     headerStyle: styles.header,
//   },
// })

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  SplashScreen: { screen: Splash },
  Language: {
    screen: Language
  },
  LaunchScreen: { screen: LaunchScreen },
  VideoDetail: {
    screen: VideoDetail
  },
  ViewAll: {
    screen: ViewAll
  }
}, {
    // Default config for all screens
    headerMode: 'none',
    initialRouteName: 'SplashScreen',
    navigationOptions: {
      headerStyle: styles.header,
    },
  })

export default PrimaryNav
